# HZB controls wiki
You can find the wiki at: [HZB Controls wiki](https://hzb-controls-wiki.readthedocs.io)

Currently only the drawio plugin is installed.

We can add more plugins, to see the whole list go to:
[Github Mkdocs plugins](https://github.com/mkdocs/catalog)

## Building
You need to have mkdocs installed:
```
pip install mkdocs mkdocs-drawio_file mkdocs-material
```
You can also  build the project on your PC with:

```
mkdocs build
```
and 
```
mkdocs serve
```

The wiiki is then hosted at [localhost:8000](localhost).
## Accessibility

This project can have a second branch, which will be accessible through pages - and won't be seen by externals.