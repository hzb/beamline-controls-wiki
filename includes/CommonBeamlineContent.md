## Start Bluesky

From a terminal write 

```bash
bluesky_start
``` 

## Start Keithleys Screens
To start the screen for the keithlys run this line in a terminal:

```bash
phoebus_start
```

Then click on the file browser button and select the four Keithleys. You can also detach the screens to be able to see the four of them together. 

## Move motors and set values (examples)

### Move motors
If you want to move a motor, or a set of motors you can simply type:

`mov motor1 position1 motor2 position2`

or using relative motions

`movr motor1 relative_motion1 motor2 relative_motion2`

A progress bar will be shown, the last number on the right is the current motor position (this works when you move only one motor)

![Progress Bar](../docs/assets/images/progress_bar_opticBL.png)


### Average measurement for each point
When measuring with the Keithleys, it is possible to instruct them to repeat the measurement *n* times, and return the averaged value.  

To do this we have to set the parameter avg_num of the Keithley to the desired value, by default is 1. For instance, to change it to 5, for each Keithley, give the following commands

```python
kth01.avg_type.set('Repeat')
kth01.avg_num.put(5)

kth02.avg_type.set('Repeat')
kth02.avg_num.put(5)

```

### Set Values
If you want to set a value, for instance a new cff for the monochromator, use the following syntax

`device.axis.set(value)`

## Change the plange grating monochromator cff

To set the cff value to 2.5 use:

`sgm.cff.set(2.5)`

or 

`pgm.cff.set(2.5)`

Note that this will not produce any movement in the monochromator. The new cff will be set once you change the energy to a new one.

## Magics
* `ct` will show a reading of all the detectors
* `wa` will show all the motor positions. You can select which motors to show using their label. For instance `wa reflectometer` will show only the reflectometer motor positions.

## Data

### Specfile
The data are exported in a spec file, located in `/home/<bluesky username>/blusky/data/beamline_commissioningYEAR`.

### Individual csv files
Additionally, individual csv files are exported in `/home/<bluesky username>/blusky/data/beamline_commissioning/csv`.

### Change User
To change the folder where the single csv files are exported use the following command:

* `changeuser("user_name")`

This will create a folder called `/home/<bluesky username>/blusky/data/user_name/csv` where the files will be exported.

Three kind of files will be exported for each scan:

* **scanNumber_baseline.csv**: here are contained the measured values for all the devices in the baseline (measured once before and once after the scan)
* **scanNumber_meta.json**: here are contained the metadata relative to the scan
* **scanNumber_primary.csv**: here are contained the data for the scan. 



## Scan Types

Several scan types are available by default in Bluesky. For a complete list and the documentation refer to the  [official documentation](https://blueskyproject.io/bluesky/plans.html)

### Original syntax
Plans have to be passed as an argument to the Run Engine, and they are functions themselfs. For instance the count plan should be invoked like this: `RE(count([noisy_det],5,delay=1))`

### Simplified syntax
Experimental ipython magics are available that aim to simplify the syntax. By calling the RE by the plan name, it is possible to use a spec-like syntax. For instance, the same count plan as above can be invoked like this: `count [noisy_det] 5 delay=1`

In case of problems, please report it to the beamline scientist and use the original syntax.

* `count`: take one or more readings from detectors. Usage (5 counts, 1 sec delay): 
    * `count [noisy_det] 5 delay=1`

* `scan` or `ascan`: scan over one multi-motor trajectory. Usage (scan motor1 between -1 and 1 in 10 points) 
    * `ascan [noisy_det] motor1 -1 1 10`

* `relative_scan` or `dscan`: scan over one multi-motor trajectory relative to current position. Usage (scan motor1 between -1 and 1 in 10 points) 
    * `dscan [noisy_det] motor1 -1 1 10`

* `list_scan`: scan over one or more variables in steps simultaneously (inner product). Usage (scan motor1 on a list of given points) 
    * `dscan [noisy_det] motor1 [point1, point2, ...]`
#### More magic...
For regular scan it is possible to simplify the syntax even more. First select the detectore, which are used most of the time and add the to the ploting detectors:
` plotseect()` 
You can check the list for ploted detectors with:
`plot_detectors`

Now you can use the plans above the with an shorte syntax:
 `count`: take one or more readings from detectors. Usage (5 counts, 1 sec delay): 
    * `count 5 1`

* `scan` or `ascan`: scan over one multi-motor trajectory. Usage (scan motor1 between -1 and 1 in 10 points) 
    * `ascan motor1 -1 1 10`

* `relative_scan` or `dscan`: scan over one multi-motor trajectory relative to current position. Usage (scan motor1 between -1 and 1 in 10 points) 
    * `dscan motor1 -1 1 10`

* `list_scan`: scan over one or more variables in steps simultaneously (inner product). Usage (scan motor1 on a list of given points) 
    * `dscan motor1 [point1, point2, ...]`

When you use the full, magics from before list, even if you have multile detect in `plot_detectors` the plan will only scan the assigned detector. For example `count [noisy_det] 5 delay=1` will can the `[noisy_det]` even if there some keithleys in `plot_detectors`.

## Settle Time (wait before measuring)
It is possible to define a settle time, which is a a number of seconds waited by a motor before reporting that the motion is completed.  For instance, to set the settle time of the `pgm.en` axis, use:

```python
pgm.en.settle_time = 5
```
### Settle Time for all motors
The following function will change the value of the settle time for all the motors to the desired value. For instance to change it to 2 or to 5 seconds use onew of the following

```python
settle_time(2)

settle_time(5)
```

## Add metadata to the scans
Custom metadata can be entered by the user at the execution time of a plan. Suppose that we are exectuing a plan called `scan()` (for brevity we omit the parameters that we need to pass to the scan function). One can add the metadata either like this:

* `RE(scan(), operator='John', sample='pure_gold')`


Another way is to add the metadata directly in the plan. In this case we need to use a python dictionary.

* `RE(scan([det], motor, 1, 5, 5, md={'operator':'John', 'sample':'gold'}))`

or using the symplified syntax:

* `scan [det] motor 1 5 5 md={'operator':'John','sample':'gold'}`


## Abort Motion
Weather you are moving a motor or running a plan, the motion can be interrupted by pressing `ctrl+c`.

This will work for the `hios` and the `pgm.en` motors. 

## Suspenders
Bluesky can automatically pause a measurement in case some conditions are met, by using `suspenders`. Two suspenders can be activated.

### wait_for_injection
This suspender pause the measurements two seconds before and after the injection happens. To activate it and deactivate it use

```python
wait_for_injections()
```

### check_beamshutter
This suspender pause the measurements if it detects that the beamshutter or the last valve are closed. To activate it and deactivate it use

```python
check_beamshutter()
```

## How to write a script
Make a folder with your name in `/home/opticsmasters/bluesky/user_scripts/your_name`. You can then write your scripts in that folder. Examples are available in that folder `/home/opticsmasters/bluesky/user_scripts/examples`

The commands that are normally written in the ipython terminal should be passed as an argument to the function `run_plan()`, as in the examples below.

One can wriute either procedural scripts, where each line will be executed one after the other one like in the example below. To run this script just type `load_user_script('examples/example_script.py'`

```python
# print the pocurrent energy of the monochromator
print(f"Current position of the PGM {pgm.en.get()}")

# move the energy to 403 eV
run_plan("mov pgm.en 403")

# run a scan
run_plan("scan [noisy_det] motor -1 1 10")
```

Or it is possible to define functions to call from the command line. To run this script just type `load_user_script('examples/example_with_functions.py')`. Now the function `test_plan()` is available in the ipython session, so just type `test_plan()` to execute it

```python
def test_plan():
    # print the pocurrent energy of the monochromator
    print(f"Current position of the PGM {pgm.en.get()}")

    # move the energy to 403 eV
    run_plan("mov pgm.en 403")

    # run a scan
    run_plan("scan [noisy_det] motor -1 1 10")
```