## Portainer

The link to the portainer has to be added to the proxy exceptions.
Portainer is availbale at [this link](172.17.0.1:9000). This address may define on the config of the docker network.


## Experts Section
If you are a user, this section is not for you. 

## Additional command to start bluesky
Bluesky is running in containers. To restart the containers use:

`bluesky_start restart` 

To bring down the containers:

`bluesky_start down` 


To enter the container to debug:

`bluesky_start sh` 

### Containers Running on the local machine
This section describes which containers are running on the local machine. If you are an user, this section is of no use for you.

To visualize the running containers, you can use either Portainer (see section above), or VSCode. 

#### Bluesky
* bluesky (The container containing the bluesky environment)
* Mongo (the database where bluesky saves the data)
* Tiled (a data-access service used to connect to the mongoDB catalog)
* Graphana (to monitor and PVs with a GUI, not necessary to run bluesky)
* MongoExpress (to monitor the usage of the MongoDB)
* Portainer (to visualize/manage the running containers) 

#### EPICS
* Phoebus