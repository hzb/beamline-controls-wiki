
## Average keithleys measurements
When measuring with the Keithleys, it is possible to instruct them to repeat the measurement *n* times, and return the averaged value.  

```python
kth1.avg_type.set('Repeat')
kth1.avg_num.put(5)
```