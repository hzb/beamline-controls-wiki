
## Data
The data is exported in a subfolder of `~/bluesky/data`.

The data is exported as specfiles (that can be visualized with PyMCA), and as individual csv files. 

Before starting taking measurements, make sure that you are exporting the data in the right folder. 

**User Data**

Change your user folder using the following command:

```python
bds.change_user('<user_name>')
```

Create an additional subfolder for each sample/part of the experiment (you need at least one).

```python
bds.change_sample('<sample_name>')
```
This is the folder structure that you create
```

    ├── ~/bluesky/data
    │   ├── user_name
    │   │   └── sample_name
    │   │   │   ├── csv
    │   │   │   │   ├── 00001_meta.json
    │   │   │   │   ├── 00001_primary.csv
    │   │   │   │   └── 00001_baseline.csv
    │   │   │   └── sample1.spec
```

**Specfile**

Specfiles can be visualized using PyMCA. To start the program click on it on the dock, or just write `pymca` in a terminal.

**Individual csv files**

The csv folder contains three files for each scan:

* **scanNumber_baseline.csv**: the baseline 
* **scanNumber_meta.json**: the metadata 
* **scanNumber_primary.csv**: the data 

**Commissioning**

To switch to the commissionig folder, use

```python
bds.commissioning()
```

