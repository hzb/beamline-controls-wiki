
## N2 Fit Function

A function to evaluate the resolving power (RP) given a nitrogen spectrum is available. The function is called `fit_n2` and it tries to automatically fit a nitrogen spectrum and calculate the resolving power. 

For instance to fit scan number 134:

  ```python
  def fit_n2(134, n_peaks=5, motor='pgm_en', detector='kth01')
  ```

change the motor and detector name accordingly. 

The function tries to fit the spectra, and it estimates the Resolving power with two methods. By calculating the Gaussian contribution to the FWHM of the N2:1s-->𝛑* transition via the fit parameters, and using a table to convert the first valley to peak ratio. The lorentian contribution by default is 114 meV. 

### How to collect data for the fit
The usual rules for fitting: 
* the more points, the better
* collect at least 0.3 eV on the left of the first peak, and on the right until you clearly see peaks.
<>
### Fit Function: SkewedVoigtModel with Linear Background

A Voigt function is the convolution of a Lorentzian and a Gaussian function. The width of the Lorentzian represent the lifetime of the 1s-->𝛑* transition, while the Guassian is an approximation of the monochromator function. The SkewdVoigtModel additionally introduces skewness to the peaks. A linear function is added to the model to account for the background. 

By deafault the Lorentzian contribution is fixed to 114 meV.

