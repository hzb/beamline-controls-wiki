### Accelerator  
label: accelerator

| Motor | Description | Units|
| ----- | ------------| ---- |
| `acc.current`  | The ring current | mA |
| `next_injection.time_next_injection` | Next injection time | sec |
