## Move motors
If you want to move a motor, or a set of motors you can simply type:

```python
mov motor1 position1 motor2 position2
```

or using relative motions

```python
movr motor1 relative_motion1 motor2 relative_motion2
```