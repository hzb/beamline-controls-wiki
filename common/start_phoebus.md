## Start Phoebus GUI
To start the screens run this line in a terminal:

```bash
phoebus_start
```

To open the gui panels navigate to the folder `/opt/phoebus/gui`.