Bluesky, is written in python, and the interface you are using is te ipython shell. The original syntax of bluesky is therefore very different from the one that we suggest in the cookbook. To achieve this simplified syntax we do an extensive use of ipython magics. 

## The Run Engine
Bluesky encodes an experimental procedure as a plan, a sequence of atomic instructions. The RunEngine is an interpreter for plans. It lets us focus on the logic of our experimental procedure while it handles important technical details consistently: it communicates with hardware, monitors for interruptions, organizes metadata and data, coordinates I/O, and ensures that the hardware is left in a safe state at exit time.

The RunEngine has already been cofigured for you, and it is available in your ipython shell. By typing `RE` in your ipython shell you will get the following

```python
In [1]: RE
Out[1]: <bluesky.run_engine.RunEngine at 0x10fd1d978>
```

## Plans
A plan is bluesky’s concept of an experimental procedure. A plan may be any iterable object (list, tuple, custom iterable class, …) but most commonly it is implemented as a Python generator.

A variety of pre-assembled plans are provided.
Several scan types are available by default in Bluesky. For a complete list and the documentation refer to the  [official documentation](https://blueskyproject.io/bluesky/plans.html)

To illustrate the differences, we compare the original syntax with the simplified version using magics commands.

| Original Syntax                   | Using Magics         |
|-----------------------------------|----------------------|
| `RE(count([det], 5))`             | `count 5`           |
| `RE(scan([det], motor, 1, 5, 5))` | `scan motor 1 5 5`  |

Our magics reconstruct the command using the original syntax, automatically inserting the correct detector within brackets.