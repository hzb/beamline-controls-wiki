Devices are instantiated from the file `~/bluesky/beamlinetools/beamlinetools/beamline_config/beamline_devices/beamline_devices.yml`. The file is written using the yml syntax. Here is an example of an element configured in all the beamlines:

```yml
- type: DetailedAccelerator
  active: true
  class: bessyii_devices.ring.DetailedAccelerator
  prefix: ""
  instance_name: accelerator
  class_kwargs: []
  connection_timeout: 5
  apply_rml_on: False
  nx_class: someNxClass
  baseline: False
  silent: True
```

## Explanation
Each device configured in this file contains a number of properties, organized in key/value pairs (e.g. `"active: True"`).  
Nine properties' keys (listed below) are mandatory for all devices since they are necessary for the instantiation process of devices.  
Two of the five properties must be unique: `"prefix"` and `"instance_name"`.  
Additional properties of the devices can be added. They are facultative.

Each property can be retrieved from an instantiated device by applying `"md"`. E.g. 
```python
accelerator.md.class
```


## Properties

| Name                | Unique | Possible Values | Description |
|---------------------|--------|----------------|-------------|
| **"type"**          | No     | Arbitrary | Device type. Best practice is to use a 'type' of object (device) from RML file if available, or alternatively, the class the device belongs to. |
| **"active"**        | No     | `{True, False}`                          | Indicates if the device is active. If active, the device will be available in Bluesky. |
| **"class"**         | No     | Arbitrary, e.g., class names             | Class name needed to instantiate the object. |
| **"prefix"**        | Yes    | Arbitrary, e.g., "PV:" or similar        | Name of the PV. |
| **"instance_name"** | Yes    | Arbitrary, e.g., "v3"                    | Instance name of the device. |
| **"class_kwargs"**  | No     | `"class_kwargs:"` or `"[]"`              | Details about class keyword arguments, if needed. If no kwargs are needed, use empty or simple key-value format. |
| **"connection_timeout"** | No | Float or Int values, e.g., `5.0`               | Timeout value used in ophyd function `wait_for_connection()`. |
| **"apply_rml_on"**  | No     | `{True, False}`                          | Determines whether to extract metadata from the RML file and incorporate it into the happi container. |
| **"nx_class"**      | No     | Arbitrary, e.g., NeXus class names       | Device class name for NeXus writer. Needed to determine which NXclass a given device instance belongs to. |
| **"baseline"**      | No     | `{True, False}`       | if `True` the device is added to the baseline |
| **"silent"**      | No     | `{True, False}`       | if `True` the device is added to the silent detectors |

### Baseline
When the Baseline property is set to True, the device's value is automatically measured both before and after each scan. This ensures that changes in the device's state are recorded in the context of each experimental run.

### Silent
Setting the Silent property to True categorizes the device as a detector that will be recorded during each scan but not automatically plotted. However, it will be included in the list of available detectors that can be manually selected for plotting. This property is useful for managing data visibility and prioritizing which measurements are highlighted during analysis.