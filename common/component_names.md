## Devices in the Beamline
The devices in the beamline have different components. To see the different component of a device use `device.component_names`. For instance, to see the component of the plane grating monochromator, called pgm, use `pgm.component_names` 

In general one can manipulate each component by referring to it as `device.component`. For example the energy of the monochromator is  `pgm.en`.
