### Suspender: Beamshutter
This suspender pause the measurements if it detects that the beamshutter or the last valve are closed. To activate it and deactivate it use

```python
check_beamshutter()
```