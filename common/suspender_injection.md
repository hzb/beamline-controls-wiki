**Injection**

This suspender pause the measurements two seconds before and after the injection happens. To activate it and deactivate it use:

```python
wait_for_injection()
```