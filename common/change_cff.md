## Change the PGM cff

To set the cff value to 2.5 use:

```python
mov pgm.cff 2.5
```

Note that this will not produce any movement in the monochromator. The new cff will be set once you change the energy to a new one.
