## Magics
| Command    | Description                                                                                    |
|------------|------------------------------------------------------------------------------------------------|
| `ct`       | Shows a reading of all the detectors.                                                          |
| `wa`       | Shows all the motor positions. You can select which motors to show using their label.          |
| `where`    | Draws a line of the current motor position in the last scan.                                   |
| `pic`      | Moves the motor to the highest value of the signal from the detector in the last scan.         |
| `cen`      | Moves the motor to the center of the signal from the detector in the last scan.                |
| `com`      | Moves the motor to the center of mass of the signal from the detector in the last scan.        |
| `minimum`  | Moves the motor to the minimum of the signal from the detector in the last scan.               |

