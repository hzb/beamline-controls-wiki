### Continuous scans
The following scan is available for motors using an EPICS motor record

```python
mov_count motor start_pos end_pos velocity
```
