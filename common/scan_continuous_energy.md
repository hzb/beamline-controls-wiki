### Continuous energy scan

This plan read detectors while flying pgm energy with start, stop, initial scan velocity, and the delay between det sample time

```python
flyscan pgm.en start=400 stop=450 vel=0.2 delay=0
```

| Parameter | Type                  | Description                                                               | Default  |
|-----------|-----------------------|---------------------------------------------------------------------------|----------|
| `detectors` | list                | A list of 'readable' objects.                                             | None     |
| `flyer`     | FlyerDevice object  | An object of FlyerDevice type.                                            | None     |
| `start`     | float               | The start value of the flyer.                                             | None     |
| `stop`      | float               | The stop value of the flyer.                                              | None     |
| `vel`       | float               | The initial velocity of the flyer.                                        | 0        |
| `delay`     | iterable or scalar  | Time delay in seconds between successive readings.                        | 0.1      |
| `shutter`   | Device              | Device with open and close value attributes, used to control a shutter.   | Optional |
| `md`        | dict                | A dictionary containing additional metadata.                              | Optional |
