## Settle Time (wait before measuring)
It is possible to define a settle time, which is a a number of seconds waited by a motor before reporting that the motion is completed.  

```python
motor.axis.settle_time = 5
```

