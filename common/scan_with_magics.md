## Scan

### Select Plotted detector
One can choose a detector to be plotted using giving the command `plotselect()`:

```python
In [1]: plotselect()
Current detector: ['kth01']
Press enter to exit
Available detectors:
1. accelerator_current
2. kth01
Select detectors by one or more numbers (e.g., '1', '1 2', '1,2')
```

### Scan Syntax

| Command  | Usage Example                                     | 
|----------|---------------------------------------------------|
| Take readings from detectors.    | `count 5 delay=1` |            |
| absolute scan one motor   | `scan  motor -1 1 10` | 
| absolute scan multiple motors  | `scan motor -1 1 motor2 -2 3 10`| 
| relative scan one motor   | `dscan  motor -1 1 10` | 
| relative mesh multiple motors  | `dmesh motor1 -1 1 10 motor2 -2 2 20`| 
| absolute mesh multiple motors  | `amesh motor1 12 14 10 motor2 18 22 20`| 



