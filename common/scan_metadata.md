## Adding Custom Metadata to Scans

Custom metadata allows users to annotate scans with additional information that can be specific to the current context or experimental conditions. This metadata is added at the time of executing a scan plan by including a dictionary named `md` as a parameter in the scan command.

Below is an example of how to add custom metadata when executing a scan. In this example, the metadata includes the name of the operator and a description of the sample:

```python
# Define the scan parameters and include custom metadata
scan motor 1 5 5 md={'operator':'John','sample':'gold'}
```