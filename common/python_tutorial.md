# Python Basics Tutorial

Welcome to this basic Python tutorial! Below, you will find essential Python concepts that are fundamental for any beginner.

## Printing Output

To print something in Python, use the `print()` function.

```python
print("Hello, World!")
```

## Variables
Variables in Python are created by assigning a value to a variable name.

```python
x = 10
name = "Alice"
```
## Lists
A list is a collection of items that can be changed.

```python
fruits = ["apple", "banana", "cherry"]
print(fruits[0]) # Accessing the first item, which is "apple"
```
## Dictionaries
A dictionary is a collection of key-value pairs.

```python
person = {"name": "Alice", "age": 25}
print(person["name"]) # Outputs: Alice
```
## Loops
Loops in Python are used to iterate over a sequence of items.

### For Loop
Use a for loop to iterate over items of a list.

```python
fruits = ["apple", "banana", "cherry"]
for fruit in fruits:
    print(fruit)
```

### While Loop
Use a while loop to repeat an action until a condition changes.

```python
count = 0
while count < 5:
    print(count)
    count += 1
```

## Conditionals
Conditional statements are used to perform different computations or actions depending on whether a condition is true or not.

```python
if age >= 18:
    print("Adult")
elif 13 < age < 18:
    print('Teenager')
else:
    print("Too young to drink")
```

## Functions
Functions are blocks of code that perform a specific task and can be reused.

```python
def greet(name):
    return f"Hello {name}!"

print(greet("Bob"))
```

