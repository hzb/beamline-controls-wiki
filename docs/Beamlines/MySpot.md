This is the cookbook for using bluesky at the mySpot Beamline. Many examples and many commands are explicitly written in a form that is easy for the user/operator to simply copy and paste them into the bluesky terminal as they are or with little modification.

## Start Bluesky

From a terminal write 

`bluesky_start` 

If it there is an error and bluesky does not start give the command again and it should work.

## Start EIGER GUI

After reboot the GUI for the eiger can be started typing this command in a terminal

`bluesky_phoebus_start`

Even if closed, it will automatically reopened. 

## Devices in the Beamline
The devices in the beamline have different components. To see the different component of a device use `device.component_names`. For instance, to see the component of the plane grating monochromator, called pgm, use `dcm.component_names` 

In general one can manipulate each component by referring to it as `device.component`. For example the energy of the monochromator is  `dcm.energy`.

### Accelerator  
label: accelerator

| Motor | Description | Units|
| ----- | ------------| ---- |
| `acc.current`  | The ring current | mA |
| `next_injection.time_next_injection` | Next injection time | sec |

### Keithleys
label: keithley

| Motor           | Description        | Units |
| --------------- | ------------------ | -------- |
| `kth00.current`      | Model 6517B   | ... |
| `kth01.current`   | Model 6517B      | ... |


### Motors
label: motors

| Motor           | Description        | Units |
| --------------- | ----[text](MySpot.md)-------------- | -------- |
| `hgx`      | ???       | mm |
| `mz`   | ???    | mm |


### Slit
label: slit

| Motor      | Description                | Units |
| ---------- | -------------------------- | -------- |
| `slit3.hgap`    | Horizontal gap           |mm|
| `slit3.vgap`    | Vertical gap             |mm|
| `slit3.hoffset` | Horizontal offset        |mm|
| `slit3.voffset` | Vertical offset          |mm|
| `slit3.top`     | Translation top blade    |mm|
| `slit3.bottom`  | Translation bottom blade |mm|
| `slit3.left`    | Translation left blade   |mm|
| `slit3.right`   | Translation right blade  |mm|

### Double Crystal Monochromator  
label: dcm

| Motor            | Description                    | Units |
| ---------        | ---------------------          | -------- |
| `dcm.p.energy`       | Energy                          |eV|
| `dcm.p.height`       | Vertical between two crystals   |a.u.|
| `dcm.p.dummy`        | Dummmy Motor, do not use it     |a.u.|
| `dcm.p.monobr`       | Bragg angle, calculated         |deg|
| `dcm.p.cr2latr`      | 2nd crystal lateral traslation  |mm|
| `dcm.p.cr2vetr`      | 2nd crystal vertical traslation |mm|
| `dcm.p.monotype`     | possible values: 'Si111','Si311','B4CML' |string|
| `dcm.p.mode`     | possible values: 'all_motors','exafs_mode','channelcut' |string|
| `dcm.p.cr2vetr`      | 2nd crystal vertical traslation |mm|
| `dcm.p.cr2vetr`      | 2nd crystal vertical traslation |mm|
| `dcm.monoz`          | Translation of both crystals    |mm|
| `dcm.monoy`          | ??? of both crystals    |mm|
| `dcm.cr1roll`        | 1st crystal roll angle          |rad|
| `dcm.cr2roll`        | 2nd crystal roll angle          |rad|
| `dcm.cr2ropi`        | 2nd crystal roll angle          |rad|
| `dcm.cr2yaw`         | 2nd crystal roll angle          |rad|
| `dcm.monobr_encoder` | Encoder value for bragg angle   |deg|

#### Set mono type
This function will select the crystal or pair or the ML mono by moving `dcm.monoz` to the correct position depending on the value of `dcm.p.monotype`.

for instance, to use the `Si111` crystal pairs use

```python
dcm.p.monotype = 'Si111'
dcm.p.set_mono_type()
```

#### Set mono mode
Three modes are available for the monochromator:

* `dcm.p.monotype = 'all_motors'` : in this case when the energy is changed the bragg angle (`dcm.p.monobr`), cr2latr (`dcm.p.cr2latr`) and cr2vetr (`dcm.p.cr2vetr`) will be moved accordingly
* `dcm.p.monotype = 'exafs'` : : in this case when the energy is changed the bragg angle (`dcm.p.monobr`), and cr2vetr (`dcm.p.cr2vetr`) will be moved accordingly. The motor cr2latr (`dcm.p.cr2latr`) remains unchanged.
* `dcm.p.monotype = 'all_motors'` : : in this case when the energy is changed only the bragg angle (`dcm.p.monobr`) will be moved accordingly. The motor cr2latr (`dcm.p.cr2latr`) and cr2vetr (`dcm.p.cr2vetr`)remain unchanged.

### Eiger

| Motor      | Description                | Info |
| ---------- | -------------------------- | -------- |
| `eiger`    | Aarea detector         |  |
| `mca`      | Fluorescence detector  |8 channels with 8 rois each|

### MCA

| Motor      | Description                | Info |
| ---------- | -------------------------- | -------- |
| `mca`      | Fluorescence detector  |8 channels with 8 rois each|

To save the data during a scan just add the mca detector to the list of detectors to be read during a scan (This is necessarz only if the mca detector was not added to the silent devices, ask the beamline scientist about it).

#### Plot a roi
To plot one of the ROI, you have to manually eanble it. For instance, to plot the roi number 3 of channel 1

`mca.ch1.roi0.display()`

More than one roi can be plotted at the same time. If you don"t want to plot it anymore:

`mca.ch1.roi0.hide()`

#### Change the aquisition time
It is possible to change the acquisition time of the detector. This has to be done before starting a scan, and not at scan time. For example, to set the acquisition time to five seconds:

`mca.acquisition_time = 5`

## Move motors and set values

### Move motors
If you want to move a motor, or a set of motors you can simply type:

`mov motor1 position1 motor2 position2`

or using relative motions

`movr motor1 relative_motion1 motor2 relative_motion2`

A progress bar will be shown, the last number on the right is the current motor position.

![Progress Bar](../assets/images/progress_bar_opticBL.png)


## Magics
* `ct` will show a reading of all the detectors
* `wa` will show all the motor positions. You can select which motors to show using their label. For instance `wa reflectometer` will show only the reflectometer motor positions.
* `where` Draws a line of the current motor position in the last scan.
* `pic` Move the motor at the highest value of the signal from the detector in the last scan
* `cen` Move the motor at the center of the signal from the detector in the last scan
* `com` Move the motor at the center of mass of the signal from the detector in the last scan
* `minimum` Move the motor at the minimum of the signal from the detector in the last scan
* `peakinfo` will show info about the peak in the last scan, if any


## Data
The data in bluesky is saved in a database called mongoDB, and then exported in different format depending on the beamline. 

Before starting to aquire data, you have to setup the your experiment (Ask the beamline scientist what to use as experiment name). 

`mds.new_experiment("experiment_name")`

Then choose a name for the data you want to aquire.

`mds.newdata("sample1_alignment")`

The data is then exported in the folder 
`/home/myspot/bluesky/data/experiment_name/sample1_alignment`

### Exported data
The data is exported in multiple files format.

#### Specfile
One specfile, contaning all the scan for the data you are acquiring is eported. It is possible to visualize historical data and compare them using PyMCA. Start PyMCA typing in the terminal:

`pymca`

#### Individual files
In the folder `csv` you find individual files for every scan, in particular:

* **scanNumber_baseline.csv**: here are contained the measured values for all the devices in the baseline (measured once before and once after the scan)
* **scanNumber_meta.json**: here are contained the metadata relative to the scan
* **scanNumber_primary.csv**: here are contained the data for the scan. 



## Scan Types

Several scan types are available by default in Bluesky. For a complete list and the documentation refer to the  [official documentation](https://blueskyproject.io/bluesky/plans.html)

### Original syntax
Plans have to be passed as an argument to the Run Engine, and they are functions themselfs. For instance the count plan should be invoked like this: `RE(count([noisy_det],5,delay=1))`

### Simplified syntax
Experimental ipython magics are available that aim to simplify the syntax. By prefixing the symbol `%` to the plan name, it is possible to use a spec-like syntax. For instance, the same count plan as above can be invoked like this: `%count [noisy_det] 5 delay=1`

In case of problems, please report it to the beamline scientist and use the original syntax.

* `count`: take one or more readings from detectors. Usage (5 counts, 1 sec delay): 
    * `%count [noisy_det] 5 delay=1`

* `scan` or `ascan`: scan over one multi-motor trajectory. Usage (scan motor1 between -1 and 1 in 10 points) 
    * `%ascan [noisy_det] motor1 -1 1 10`

* `relative_scan` or `dscan`: scan over one multi-motor trajectory relative to current position. Usage (scan motor1 between -1 and 1 in 10 points) 
    * `%dscan [noisy_det] motor1 -1 1 10`

* `list_scan`: scan over one or more variables in steps simultaneously (inner product). Usage (scan motor1 on a list of given points) 
    * `%dscan [noisy_det] motor1 [point1, point2, ...]`

* `exafs`: to be documented yet


## Settle Time (wait before measuring)
It is possible to define a settle time, which is a a number of seconds waited by a motor before reporting that the motion is completed.  For instance, to set the settle time of the `dcm.p.energy` axis, use:

```python
dcm.p.energy = 5
```

## Add metadata to the scans
Custom metadata can be entered by the user at the execution time of a plan. Suppose that we are exectuing a plan called `scan()` (for brevity we omit the parameters that we need to pass to the scan function). One can add the metadata either like this:

* `RE(scan(), operator='John', sample='pure_gold')`


Another way is to add the metadata directly in the plan. In this case we need to use a python dictionary.

* `RE(scan([det], motor, 1, 5, 5, md={'operator':'John', 'sample':'gold'}))`

or using the symplified syntax:

* `%scan [det] motor 1 5 5 md={'operator':'John','sample':'gold'}`


## Abort Motion
Weather you are moving a motor or running a plan, the motion can be interrupted by pressing `ctrl+c`.


## Suspenders
**This chapter has to be reviewed**
Bluesky can automatically pause a measurement in case some conditions are met, by using `suspenders`. Two suspenders can be activated.

### check_beamshutter
This suspender pause the measurements if it detects that the beamshutter or the last valve are closed. To activate it and deactivate it use

```python
check_beamshutter()
```

## How to write a script
Make a folder with your name in `/home/opticsmasters/bluesky/user_scripts/your_name`. You can then write your scripts in that folder. Examples are available in that folder `/home/opticsmasters/bluesky/user_scripts/examples`

The commands that are normally written in the ipython terminal should be passed as an argument to the function `run_plan()`, as in the examples below.

One can wriute either procedural scripts, where each line will be executed one after the other one like in the example below. To run this script just type `load_user_script('examples/example_script.py'`

```python
# print the pocurrent energy of the monochromator
print(f"Current position of the PGM {pgm.en.get()}")

# move the energy to 403 eV
run_plan("%mov pgm.en 403")

# run a scan
run_plan("%dscan [noisy_det] motor -1 1 10")
```

Or it is possible to define functions to call from the command line. To run this script just type `load_user_script('examples/example_with_functions.py')`. Now the function `test_plan()` is available in the ipython session, so just type `test_plan()` to execute it

```python
def test_plan():
    # print the pocurrent energy of the monochromator
    print(f"Current position of the PGM {pgm.en.get()}")

    # move the energy to 403 eV
    run_plan("%mov pgm.en 403")

    # run a scan
    run_plan("%dscan [noisy_det] motor -1 1 10")
```


<!-- 
## Portainer

Portainer is availbale at [this link](https://localhost:9443/#!/2/docker/containers/770b984d0699618a12439ab492931540bacec834ee2841b78850490e4427bf44).

Pw should be the same of the pc + 123, I dont remember the username -->



## Experts Section
If you are a user, this section is not for you. 

## Additional command to start bluesky
Bluesky is running in containers. To restart the containers use:

`bluesky_start restart` 

To bring down the containers:

`bluesky_start down` 


To enter the container to debug:

`bluesky_start sh` 

### Containers Running on the local machine
This section describes which containers are running on the local machine. If you are an user, this section is of no use for you.

To visualize the running containers, you can use either Portainer (see section above), or VSCode. 

#### Bluesky
* bluesky (The container containing the bluesky environment)
* Mongo (the database where bluesky saves the data)
* Tiled (a data-access service used to connect to the mongoDB catalog)
* Graphana (to monitor and PVs with a GUI, not necessary to run bluesky)
* MongoExpress (to monitor the usage of the MongoDB)
* Portainer (to visualize/mange the running containers) 

