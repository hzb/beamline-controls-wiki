# Aquarius

<style>
body {
  font-family: Arial, sans-serif;
  background: linear-gradient(rgba(255, 255, 255, 0.8), rgba(255, 255, 255, 0.8)), 
              url('https://example.com/background-image.jpg');
  background-size: cover;
  background-position: center;
}

.grid-container {
  display: grid;
  grid-template-columns: 1fr 1fr;
  grid-template-rows: 1fr 1fr;
  grid-gap: 30px;
  text-align: center;
  padding: 60px;
  font-size: 1.2em;
}

.grid-item {
  padding: 40px;
  border: 2px solid #ddd;
  border-radius: 15px;
  box-shadow: 0px 4px 10px rgba(0, 0, 0, 0.1);
  transition: transform 0.3s ease, box-shadow 0.3s ease;
  background-color: rgba(255, 255, 255, 0.85);  /* Transparent background to blend with the page */
  backdrop-filter: blur(10px); /* Adds a glass-like effect */
}

.grid-item:hover {
  transform: translateY(-10px);
  box-shadow: 0px 6px 12px rgba(0, 0, 0, 0.2);
}

.grid-item strong {
  font-size: 1.6em;
  color: #333;
}

.grid-item em {
  color: #ff7043;
  font-weight: 600;
  font-size: 1.3em;
}

.grid-item a {
  color: #1e88e5;
  text-decoration: none;
  font-weight: 500;
  margin-top: 10px;
  display: inline-block;
  transition: color 0.2s ease;
}

.grid-item a:hover {
  text-decoration: underline;
  color: #005cb2;
}

.grid-item a::before {
  content: "➡ ";
}

/* Additional styling for page */
h1 {
  font-size: 2.5em;
  text-align: center;
  color: #3c3c3c;
  margin-bottom: 40px;
}

h1::after {
  content: '';
  display: block;
  width: 50px;
  height: 4px;
  background-color: #ff7043;
  margin: 10px auto;
}
</style>

<div class="grid-container">
  <div class="grid-item">
    <strong>HOW-TO GUIDES</strong><br>
    <a href="#cookbook">Cookbook</a><br>
  </div>

  <div class="grid-item">
    <strong>DEVICES</strong><br>
    <a href="#devices">Enter</a><br>
  </div>
</div>

## Cookbook

This is the cookbook for using bluesky at the Belchem Beamline. Many examples and many commands are explicitly written in a form that is easy for the user/operator to simply copy and paste them into the bluesky terminal as they are or with little modification.

{%
    include-markdown "../../common/start_bluesky.md"
%}

{%
    include-markdown "../../common/start_phoebus.md"
%}

{%
    include-markdown "../../common/move_motors.md"
%}

{%
    include-markdown "../../common/change_cff.md"
%}

{%
    include-markdown "../../common/scan_with_magics.md"
%}

{%
    include-markdown "../../common/scan_continuous.md"
%}

{%
    include-markdown "../../common/scan_continuous_energy.md"
%}

{%
    include-markdown "../../common/scan_metadata.md"
%}

{%
    include-markdown "../../common/abort_motion.md"
%}

{%
    include-markdown "../../common/magics.md"
%}

{%
    include-markdown "../../common/data_management.md"
%}

{%
    include-markdown "../../common/average_keithleys.md"
%}

### Suspenders

Bluesky can automatically pause a measurement in case some conditions are met, by using `suspenders`.

{%
    include-markdown "../../common/suspender_injection.md"
%}

{%
    include-markdown "../../common/suspender_beamshutter.md"
%}

{%
    include-markdown "../../common/write_a_script.md"
%}

## Devices

{%
    include-markdown "../../common/component_names.md"
%}

{%
    include-markdown "../../common/accelerator.md"
%}

### Keithleys

label: apertures

| Motor           | Description        | Units | Port|
| --------------- | ------------------ | -------- | -------|
| `kth01.current`   | Model 6517B   | ... | 9001 |
| `kth02.current`   | Model 6514       | ... | 9002 |

### Water Apertures  

label: apertures

| Motor           | Description        | Units |
| --------------- | ------------------ | -------- |
| `au.top`      | Top aperture       | mm |
| `au.bottom`   | Bottom aperture    | mm |
| `au.left`     | Left aperture      | mm |
| `au.right`    | Right aperture     | mm |

### First Mirror

label: mirrors

| Motor      | Description                | Units |
| ---------- | -------------------------- | -------- |
| `m1.tx`    | Translation in X           |µm|
| `m1.ty`    | Translation in Y           |µm|
| `m1.tz`    | Translation in Z           |µm|
| `m1.rx`    | Rotation about X axis      |rad|
| `m1.ry`    | Rotation about Y axis      |rad|
| `m1.rz`    | Rotation about Z axis      |rad|

### Plane Grating Monochromator

label: pgm

| Motor            | Description                    | Units |
| ---------        | ---------------------          | -------- |
| `pgm.en`         | Energy                         |eV|
| `pgm.cff`        | Constant fix focus factor      |a.u.|
| `pgm.diff_order` | Diffraction Order              |a.u.|
| `pgm.slitwidth`  | Exit Slit width                |µm|
| `pgm.grating`    | The grating that is being used |l/mm|

## Moxa Box

The Moxa Box is reachable at the IP `172.31.229.33` and the user `admin`.
