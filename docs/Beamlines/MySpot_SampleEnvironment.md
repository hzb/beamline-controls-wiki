This is the cookbook for using the bluesky installation for controlling the sample environment at the mySpot Beamline. Many examples and many commands are explicitly written in a form that is easy for the user/operator to simply copy and paste them into the bluesky terminal as they are or with little modification.

## Start Bluesky

From a terminal write 

`bluesky_sample_environment` 

If it there is an error and bluesky does not start give the command again and it should work.

## Devices in the Beamline
The devices in the beamline have different components. To see the different component of a device use `device.component_names`. For instance, to see the component of the plane grating monochromator, called pgm, use `dcm.component_names` 

In general one can manipulate each component by referring to it as `device.component`. For example the energy of the monochromator is  `dcm.energy`.

### Accelerator  
label:

| Motor | Description | Units|
| ----- | ------------| ---- |
| `acc.current`  | The ring current | mA |
| `next_injection.time_next_injection` | Next injection time | sec |


## Move motors and set values

### Move motors
If you want to move a motor, or a set of motors you can simply type:

`mov motor1 position1 motor2 position2`

or using relative motions

`movr motor1 relative_motion1 motor2 relative_motion2`

A progress bar will be shown, the last number on the right is the current motor position.

![Progress Bar](../assets/images/progress_bar_opticBL.png)


## Magics
* `ct` will show a reading of all the detectors
* `wa` will show all the motor positions. You can select which motors to show using their label. For instance `wa reflectometer` will show only the reflectometer motor positions.
* `where` Draws a line of the current motor position in the last scan.
* `pic` Move the motor at the highest value of the signal from the detector in the last scan
* `cen` Move the motor at the center of the signal from the detector in the last scan
* `com` Move the motor at the center of mass of the signal from the detector in the last scan
* `minimum` Move the motor at the minimum of the signal from the detector in the last scan
* `peakinfo` will show info about the peak in the last scan, if any


## Data
**This chapter must be reviewed**
### Specfile
The data are exported in a spec file, located in `/home/myspot/bluesky/data/beamline_commissioningYEAR`.

### Individual csv files
Additionally, individual csv files are exported in `/home/opticsmaster/blusky/data/beamline_commissioning/csv`.

### Change User
To change the folder where the single csv files are exported use the following command:

* `changeuser("user_name")`

This will create a folder called `/home/myspot/bluesky/data/user_name/csv` where the files will be exported.

Three kind of files will be exported for each scan:

* **scanNumber_baseline.csv**: here are contained the measured values for all the devices in the baseline (measured once before and once after the scan)
* **scanNumber_meta.json**: here are contained the metadata relative to the scan
* **scanNumber_primary.csv**: here are contained the data for the scan. 



## Scan Types

Several scan types are available by default in Bluesky. For a complete list and the documentation refer to the  [official documentation](https://blueskyproject.io/bluesky/plans.html)

### Original syntax
Plans have to be passed as an argument to the Run Engine, and they are functions themselfs. For instance the count plan should be invoked like this: `RE(count([noisy_det],5,delay=1))`

### Simplified syntax
Experimental ipython magics are available that aim to simplify the syntax. By prefixing the symbol `%` to the plan name, it is possible to use a spec-like syntax. For instance, the same count plan as above can be invoked like this: `%count [noisy_det] 5 delay=1`

In case of problems, please report it to the beamline scientist and use the original syntax.

* `count`: take one or more readings from detectors. Usage (5 counts, 1 sec delay): 
    * `%count [noisy_det] 5 delay=1`

* `scan` or `ascan`: scan over one multi-motor trajectory. Usage (scan motor1 between -1 and 1 in 10 points) 
    * `%ascan [noisy_det] motor1 -1 1 10`

* `relative_scan` or `dscan`: scan over one multi-motor trajectory relative to current position. Usage (scan motor1 between -1 and 1 in 10 points) 
    * `%dscan [noisy_det] motor1 -1 1 10`

* `list_scan`: scan over one or more variables in steps simultaneously (inner product). Usage (scan motor1 on a list of given points) 
    * `%dscan [noisy_det] motor1 [point1, point2, ...]`

* `exafs`: to be documented yet



## Add metadata to the scans
Custom metadata can be entered by the user at the execution time of a plan. Suppose that we are exectuing a plan called `scan()` (for brevity we omit the parameters that we need to pass to the scan function). One can add the metadata either like this:

* `RE(scan(), operator='John', sample='pure_gold')`


Another way is to add the metadata directly in the plan. In this case we need to use a python dictionary.

* `RE(scan([det], motor, 1, 5, 5, md={'operator':'John', 'sample':'gold'}))`

or using the symplified syntax:

* `%scan [det] motor 1 5 5 md={'operator':'John','sample':'gold'}`


## Abort Motion
Weather you are moving a motor or running a plan, the motion can be interrupted by pressing `ctrl+c`.

