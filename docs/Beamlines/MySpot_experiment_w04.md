
# MySpot Experiment Week 04

## Starting the GUI

You can control the beamline using the GUI. To start the GUI, execute in a terminal the following command:

`bluesky_gui_secondary`

## Creating a new experiment

Your data is saved in folders named after your experiment and measurements within those. There is a tab in the GUI for configuring this called "Data Structure".

![alt text](images/mySpot/using_metadata_tab.gif)

Enter the name of your experiment which will create the top level directory for storing the data your produce. You can then create any number of sub directories for sample or other studies under that.

When you make a new dataset/experiment the `scan_id` is automatically reset to 0.

```
/home/myspot/bluesky/data/
|
|___2024-12-03_experiment_name/
    |
    |---2024-12-03_dataset_1/
    |   |
    |   |---2024-12-03_dataset_1.spec
    |   |---mca
    |       |___2024-12-03_dataset_1_{scan_id}_{event_num}.h5
    |
    |---2024-12-03_dataset_2/
        |
        |---2024-12-03_dataset_2.spec
        |---mca
            |___2024-12-03_dataset_2_{scan_id}_{event_num}.h5
```
s
## Adding plans to the Queue

On the left side of the screen is a window that allows you to configure various plans. The drop down menu gives a list of all available plans. 

You can enter the parameters you need, then press "Submit" the plan will then be added to the queue. You can keep pressing submit and it will be repeated.

![alt text](images/mySpot/using_the_queue.gif)

## Controlling the Queue

There are a number of buttons at the top of the queue. Here is what each one does, in the order it appears.

| Name |      Function      |  Enabled When |
|----------|-------------|------|
| Play|  Start the queue if it is not running, or resume a plan if it is paused | The environment is open and there is a plan in the queue or paused. |
| Pause |  Pause a plan at the next possible moment if it is running. If a motor (e.g DCM) is moving, it will wait until the move is finished. For a long move (e.g. change from 9KeV to 18 KeV) this might take 12 mins. *Please be patient, it's not just software* | A plan is running |
| Stop |   Request that the queue stops after the next plan has ended.  |  A plan is running |
| Skip | [Pause the current plan](https://codebase.helmholtz.cloud/hzb/bluesky/qt_gui/source/bessy-bluesky-widgets/-/blob/main/bessyii_bluesky_widgets/qt/QtQueueControl.py#L553), stop the RE, then move to the next item in the queue. |   A plan is running |
| Loop | Request that the queue runs in loop mode. Plans that have finished executing will be returned to the back of the queue. It will keep looping until you stop the queue |   A plan is running |
| Copy | Duplicate selected items in the queue |   Always |
| Edit | Copy the contents of a selected plan to the plan editor and remove this item from the queue. Pressing submit again will put it back where it was. |   Always |
| Delete | Remove a selected item from the queue |   Always |
| Batch | Save the selected set of plans as a named batch |   Always |
| Load | Load a batch of plans from a file |   Always |
| Clear | Clear the plans from the queue |   Always |

## Monitoring the Running Plan

The box above the plan editor and the queue control widget allows you to monitor the currently running plan. The start time is shown along with the plan name and arguments. 

![alt text](images/mySpot/progress_bar.png)

## Live Plotting

Data from each process (sample environment and beamline) is plotted seperately. Plotting for both is configured to run automatically. For reference, here are the names of the services which are doing the plotting:

| Desription | Service Name | Bash command to start |
|----------|-------------|------|
| Plotting from the beamline process (e.g mca counts) | `bluesky_plotting_callback` | `bluesky_plotting_secondary` |
| Plotting from the sample environment process (e.g temperature, pressure) | `bluesky_se_liveplotting_callback` | `bluesky_se_live_plotting` | 

## Notepad GUI

So that it's possible to construct batches, another gui is provided which will not run plans. This is a notepad for you to work with while other measurements are running.

You can start this with:

`bluesky_gui_notepad`

![alt text](images/mySpot/notepad_gui.png)

## Jupyter Hub

There is a jupyter hub provided at http://evalc1e2s2x.exp0202.bessy.de/ For now this is only accessible inside the experiment network. There is a LAN cable provided if you need access to that from another machine.

The username is `beamline_user` the password is `bluesky`

## Motors mapping

- Sample Z (Vertical): *hgz*

- Sample X (Horizontal): *kox*

- Sample Tilt: *tilt*

## Read the energy

Due to the software design of the DCM, the only reliable way of reading the DCM Energy is from the IPython console. 

From the GUI, click on the left side, click on the *"IPython Console"* tab. Click on the button *"Connect to Kernel"*, press *ENTER* and then type:

```
energy.read()
```

## Plans

### Generic Beamline Plans

#### mov_dcm_energy

Change the energy to selected value.

#### mov_motors

Moves a motor to selected position.

*Parameters:*

- *motors*: the motor you want to move.
- *position*: where do you want to move the motor.
- *md*: add a comment to metadata

#### ascan_motors

Performs a scan over a motor and read from a list of detectors.

*Parameters:*

- *detectors*: detectors to read from during the scan. Select from the list. You can select multiple detectors.
- *motors*: the motor you want to move.
- *start*: start position of the scan.
- *stop*: stop position. The motor will be at this position at the end of the scan.
- *num*: Numbers of equally spaced steps between *start* and *stop*.
- *md*: add a comment to metadata

#### count_detectors

Reads from a list of detectors.

*Parameters:*

- *detectors*: detectors to read from during the scan. Select from the list. You can select multiple detectors.
- *num*: numbers of consecutive readings.
- *delay*: delay in seconds after each measurement.
- *md*: add a comment to metadata

## Data

### Where is my data?

You can find the your directory structure in the following path: `/home/bluesky/data`.

### pyMCA

pymca is installed on the machine and is probably the easiest way of viewing historical data from an experiment. Remember you will need to navigate to your dataset and that it has a type that is not the default.

If it's not open you can start pymca by opening a terminal and running `pymca` or by pressing the icon in the start bar on the left.
