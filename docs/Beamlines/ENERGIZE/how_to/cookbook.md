# Cookbook

{%
    include-markdown "../../../../common/start_bluesky.md"
%}

{%
    include-markdown "../../../../common/start_phoebus.md"
%}

{%
    include-markdown "../../../../common/move_motors.md"
%}

{%
    include-markdown "../../../../common/change_cff.md"
%}

{%
    include-markdown "../../../../common/scan_with_magics.md"
%}

{%
    include-markdown "../../../../common/settle_time.md"
%}

{%
    include-markdown "../../../../common/average_keithleys.md"
%}

{%
    include-markdown "../../../../common/scan_metadata.md"
%}

{%
    include-markdown "../../../../common/abort_motion.md"
%}

{%
    include-markdown "../../../../common/magics.md"
%}

{%
    include-markdown "../../../../common/data_management.md"
%}

## Suspenders

Bluesky can automatically pause a measurement in case some conditions are met, by using `suspenders`.

{%
    include-markdown "../../../../common/suspender_injection.md"
%}
