{%
    include-markdown "../../../../common/component_names.md"
%}

{%
    include-markdown "../../../../common/accelerator.md"
%}

### Aperture

label: apertures

| Motor           | Description        | Units |
| --------------- | ------------------ | -------- |
| `au.top`      | Top aperture       | mm |
| `au.bottom`   | Bottom aperture    | mm |
| `au.left`     | Left aperture      | mm |
| `au.right`    | Right aperture     | mm |

### M1

label: mirrors

| Motor      | Description                | Units |
| ---------- | -------------------------- | -------- |
| `m1.tx`    | Translation in X           |µm|
| `m1.ty`    | Translation in Y           |µm|
| `m1.rx`    | Rotation about X axis      |rad|
| `m1.ry`    | Rotation about Y axis      |rad|
| `m1.rz`    | Rotation about Z axis      |rad|
| `m1.temperature`    | temperature      |°C|

### Plane Grating Monochromator

label: pgm

| Motor            | Description                    | Units |
| ---------        | ---------------------          | -------- |
| `pgm.en`         | Energy                         |eV|
| `pgm.cff`        | Constant fix focus factor      |a.u.|
| `pgm.diff_order` | Diffraction Order              |a.u.|
| `pgm.slit`       | Exit Slit width                |µm|
| `pgm.grating`    | The grating that is being used |l/mm|
| `pgm.beta`       | The incident exit angle on the grating |deg|
| `pgm.theta`      | The incident angle on the mirror |deg|

### Keithleys

label: keithley
Two Keithleys are available with names `kth00`(model 6517) and `kth01` (model 6514). Each device has the following components:

| Motor           | Description        | Units | Values |
| --------------- | ------------------ | -------- | -------- |
| `kth(xx).readback`      | current   | Amp |  |
| `kth(xx).rnge`      | range   | n.a. | |
| `kth(xx).nplc`      | number of power line cycles   | n.a. |0.01 (fast) to 10 (accurate) |
| `kth(xx).int_time`      | integration time   | n.a. ||
| `kth(xx).avg_num`      | number of averages    | n.a. |1-100 readings|
| `kth(xx).avg_type`      | average type    | n.a. |0:moving average, 1:repeating average|
| `kth(xx).vsrc_ena`      | voltage enabled    | n.a. |0:no, 1:yes|
| `kth(xx).vsrc`      | voltage     | n.a. |40 V to 400 V|

### Focus Chamber

label: chamber
The Focus Chamber device at the moment is **READ ONLY**

| Motor            | Description                    | Units |
| ---------        | ---------------------          | -------- |
| `chamber.Rail`       | Rail position distance along the beam (read only)    |mm|
| `chamber.Scale`      | Current image conversion factor            |micron/pixel|
| `chamber.Tilt`       | Rotation of an ellipse calculated by an elliptical fit to the spot          |degree|
| `chamber.Camera`     | Camera Positions (x,y)                     |pixel number|
| `chamber.Wpos`       | Position of FWHM based calculation (h,v)   |pixel number|
| `chamber.Wfwhm`      | FWHM value of FWHM based calculation (h,v) |pixel number|
| `chamber.Gpos`       | Position of Gauss Fit (h,v)                |pixel number|
| `chamber.Gsigma`     | Sigma value of Gauss Fit (h,v)             |pixel number|
| `chamber.Mpos`       | First moment of (h,v) distributions        |pixel number|
| `chamber.Msigma`     | Second moment of (h,v) distributions       |pixel number|
| `chamber.Cpos`       | Position of Centroids (h,v)                |pixel number|
| `chamber.sum_intensity`| Sum of the intensity         | |
| `chamber.max_intensity`| Max of the intensity         | |

The `chamber.Camera` component has two signals, one for each position, accessible using `chamber.Camera.x` and `chamber.Camera.y`.  
The other components marked with (h,v) have a signal for each value, accessible using `.v` and `.h` (e.g. `chamber.Wpos.v` and `chamber.Wpos.h`).
