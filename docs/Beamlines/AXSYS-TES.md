<style>
body {
  font-family: Arial, sans-serif;
  background: linear-gradient(rgba(255, 255, 255, 0.8), rgba(255, 255, 255, 0.8)), 
              url('https://example.com/background-image.jpg');
  background-size: cover;
  background-position: center;
}

.grid-container {
  display: grid;
  grid-template-columns: 1fr 1fr;
  grid-template-rows: 1fr 1fr;
  grid-gap: 30px;
  text-align: center;
  padding: 60px;
  font-size: 1.2em;
}

.grid-item {
  padding: 40px;
  border: 2px solid #ddd;
  border-radius: 15px;
  box-shadow: 0px 4px 10px rgba(0, 0, 0, 0.1);
  transition: transform 0.3s ease, box-shadow 0.3s ease;
  background-color: rgba(255, 255, 255, 0.85);  /* Transparent background to blend with the page */
  backdrop-filter: blur(10px); /* Adds a glass-like effect */
}

.grid-item:hover {
  transform: translateY(-10px);
  box-shadow: 0px 6px 12px rgba(0, 0, 0, 0.2);
}

.grid-item strong {
  font-size: 1.6em;
  color: #333;
}

.grid-item em {
  color: #ff7043;
  font-weight: 600;
  font-size: 1.3em;
}

.grid-item a {
  color: #1e88e5;
  text-decoration: none;
  font-weight: 500;
  margin-top: 10px;
  display: inline-block;
  transition: color 0.2s ease;
}

.grid-item a:hover {
  text-decoration: underline;
  color: #005cb2;
}

.grid-item a::before {
  content: "➡ ";
}

/* Additional styling for page */
h1 {
  font-size: 2.5em;
  text-align: center;
  color: #3c3c3c;
  margin-bottom: 40px;
}

h1::after {
  content: '';
  display: block;
  width: 50px;
  height: 4px;
  background-color: #ff7043;
  margin: 10px auto;
}
</style>

<div class="grid-container">
  <div class="grid-item">
    <strong>HOW-TO GUIDES</strong><br>
    <a href="#cookbook">Cookbook</a><br>
    <a href="#tes-calibration">TES Calibration</a><br>
    <a href="#data-storage">Data Storage</a><br>
  </div>

  <div class="grid-item">
    <strong>DEVICES</strong><br>
    <a href="#devices">Enter</a><br>
  </div>
</div>

## Cookbook

This is the cookbook for using bluesky at the Belchem Beamline. Many examples and many commands are explicitly written in a form that is easy for the user/operator to simply copy and paste them into the bluesky terminal as they are or with little modification.

{%
    include-markdown "../../common/start_bluesky.md"
%}

{%
    include-markdown "../../common/start_phoebus.md"
%}

{%
    include-markdown "../../common/move_motors.md"
%}

{%
    include-markdown "../../common/change_cff.md"
%}

## Preparing TES for measurments

The TES control system is running on two computers:

| hostname                              | IP                |   usage                                     |
|   -----                               |   --              |   ----                                      |
|  tessy.exp0902.bessy.de               |   173.31.133.40   |  TES raw data and TES server environment    |
|  bluesky-exp0902.exp0902.bessy.de     |   173.31.133.40   |  bluesky environment (nbs-gui, queueserver) |

The `nbs-gui` is connected to the TES through th `tes_server` which needs to run on _tessy.exp0902.bessy.de_. 

Starting procedure for the `tes_server`:

**on _tessy.exp0902.bessy.de_**

1. start dastard, type the follwoing in a terminal: 

```bash
dastard
```

2. start the `tes_server`, for this you can use a small GUI, type the following in a terminal:

```bash
tes_gui
```
3. A small GUI with 3 buttons will open, click on `Start all TES`, this will open `dcom` and start the `tes_server` (a caproto IOC). 

When everything went good, the last two buttons should become green and you should be able to see count in the microscope software.

**on _bluesky-exp0902.exp0902.bessy.de_**

Normally nothing need to be done, since the `nbs-gui` and a docker container `tes_caproto` (this one start automaticlly on boot) should handle everything for you.

## Start bluesky GUI

The GUI to control the experiment is `nbs-gui`, it can be started by typing this line in a terminal:

```bash
nbs-gui-start
```

This GUI uses bluesky queueserver, queueserver is automatically started in a docker container on the boot and should be always running.

Connect the `nbs-gui` to the queueserver by click on `connect` on the `Queue Control tab`. When the connection process is done open the 
environment by click on `open` button in the Environment group. 

When everything worked the head of your `Queue Control` should looks like this:

![open env](../assets/images/axsys_gui/open_env_nsb_gui.png)

You are now connected to the bluesky environment of the queueserever were all need actions for the experiment can be started.

## TES calibration

Before a scan or any measurment is take you need to run a calibration for the TES. This calbration is save is saved in the same directory as the measurments data it belongs to,the calibration is need for data procesing.

To start the calibration go to `TES Control Tab` and do the followong steps:

1. close the beam shutter and click on `Take noise`
2. open the beam shutter again and click on `Take Projectors`
3. Click on `Make and Send Projectors`

Now you can run a `TES calibrate` plan on the `Plan Editor tab`.

## Running plans with GUI

Predefined plans for `axsystes` can be run inthe `Plan Editor tab` you can run the follwoing plan from `nbs-gui`:

|   plan name     |  description                                                            |
|  -------------  |   --------------------------------------------------------------------- |
|   TES Calibrate |   TES  detector calibration, noise and projectore need to be done before|
|   move          |   move to apature or sgm                                                |
|   scan          |   run a scan plan                                                       |
|   timescan      |   run a timed scan                                                      |
|   XAS           |   run a XAS scan                                                        |

In this tab plan can be queued, so the plans are started by the queueserver in a sertain order. The queue can be manipulated, started and stop by the queue controls on the right.

![plan tab](../assets/images/axsys_gui/plan_tab.png)

Choosse a plan fill in the parameter press `Submi` to add the plan to the queue, press `Start` at the `Queue` groub on the right to start the queue. On the
`Queue Control` you can see which plan is correntlu executed.

## Connect to the ipython console

On the `IPython Console tab` of the GUI you can connect to the RE and use the ipython console to controle bluesky.
Switch to the `IPython Console tab` and click the `Connect to Kernel` button, for this the environment of the queueserver needs to be open (see [Start bluesky GUI](#start-bluesky-gui) section).

An open ipython console looks like this, you can use the usual bluesky commands:
![open ipython](../assets/images/axsys_gui/ipython_console.png)

## Dat visialisation with nbs-viewer

To look into measure data (unprocessed) during the experimnent youi can use `nbs-viewer`, to start it type the following into the terminal: 

```bash
nbs-viewer
```

Click on `New Data Source` to connect to the tiled service (default its: _http://localhost:8000_). Choose the time peroide for the data you want to be displayed and click on `Display Selection`.
Select the `scan_id` on the left you would like to plot data from. On the right side you can check wich data should be ploted.

![nbs-viewer](../assets/images/axsys_gui/nbs_viewer.png)

## Data storage

Bluesky metadata are stored in a MongoDB and can be accessed by `tiled` and in a _spec file_ on _tessy.exp0902.bessy.de_. TES data, raw and processed are saved local on _tessy.exp0902.bessy.de_(for now!).

Metadata:

_spec file_ in `~/bluesky/data/`   can be opened with pymca

TES data:

_raw data_ in `~/data/<date of measurment>/`

_precessed data_ in `~/analysis/<date of measurment>/`

## Processing data

Raw data on _tessy.exp0902.bessy.de_ can be processed by using some python scripts.

To start the ipython environment to process data type the following command into the terminal:

```bash
cd ~/analysis/
ipython --profile analysis
```

To processe a XAS scan you first ned to load the calibration which belongs to this scan:

to select a scan type:

```python
sa.advance_to_index("<sacn_id or uid of the scan>")
```

You can look into `nbs-viewer` to find the _scan_id_ or the _uid_.

Once you selected the calibration analze it with:

```python
sa.handle_run()
```

this take some times

To process a a scan usinf the calibration, selct the scan first:

```python
sa.advance_to_index("<sacn_id or uid of the scan>")
```

and run the analysis again with:

```python
sa.handle_run()
```

Now you can plot the result and save them into `~/analysis/<date of the scan>`

Ploting can be done with:

```python
sa.plot_emission(480,520)()
sa.plot_scan2d(690,760)
sa.plot_scan21d(690,760)
```

## Devices

{%
    include-markdown "../../common/component_names.md"
%}

{%
    include-markdown "../../common/accelerator.md"
%}

### Accelerator

label: accelerator

| Motor | Description | Units|
| ----- | ------------| ---- |
| `acc.current`  | The ring current | mA |
| `next_injection.time_next_injection` | Next injection time | sec |

### Keithleys

label: apertures

| Motor           | Description        | Units |
| --------------- | ------------------ | -------- |
| `kth01.current`      | Model 6517B   | ... |
| `kth02.current`   | Model 6517B      | ... |

### Water Apertures

label: apertures

| Motor           | Description        | Units |
| --------------- | ------------------ | -------- |
| `wau1.top`      | Top aperture       | mm |
| `wau1.bottom`   | Bottom aperture    | mm |
| `wau1.left`     | Left aperture      | mm |
| `wau1.right`    | Right aperture     | mm |

### First Mirror

label: mirrors

| Motor      | Description                | Units |
| ---------- | -------------------------- | -------- |
| `m1.tx`    | Translation in X           |µm|
| `m1.ty`    | Translation in Y           |µm|
| `m1.tz`    | Translation in Z           |µm|
| `m1.rx`    | Rotation about X axis      |rad|
| `m1.ry`    | Rotation about Y axis      |rad|
| `m1.rz`    | Rotation about Z axis      |rad|

### Plane Grating Monochromator

label: pgm

| Motor            | Description                    | Units |
| ---------        | ---------------------          | -------- |
| `sgm.en`         | Energy                         |eV|
| `sgm.cff`        | Constant fix focus factor      |a.u.|
| `sgm.diff_order` | Diffraction Order              |a.u.|
| `sgm.slitwidth`  | Exit Slit width                |µm|
| `sgm.grating`    | The grating that is being used |l/mm|

## Moxa Box

The Moxa Box is currently configer with the IP `172.31.133.222`.
