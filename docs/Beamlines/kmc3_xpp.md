# KMC3-XPP

This is the cookbook for using bluesky at the KMC3-XPP Beamline. Many examples and many commands are explicitly written in a form that is easy for the user/operator to simply copy and paste them into the bluesky terminal as they are or with little modification.


{% include-markdown "../../includes/CommonBeamlineContent.md" %}

## Devices in the Beamline
The devices in the beamline have different components. To see the different component of a device use `device.component_names`. For instance, to see the component of the plane grating monochromator, called pgm, use `pgm.component_names` 

In general one can manipulate each component by referring to it as `device.component`. For example the energy of the monochromator is  `pgm.en`.

### Accelerator  
label: accelerator

| Motor | Description | Units|
| ----- | ------------| ---- |
| `acc.current`  | The ring current | mA |
| `next_injection.time_next_injection` | Next injection time | sec |

### Eiger2
label: apertures

Eiger2 bluesky device has the following plugins enabled:

* cam
* roi1
* roi2
* stats1
* stats2

Once we start the EIGER2, the EIGER2 IOC should be restarted. The initialization will be done by the IOC.
At the first start, it's neccessary to enable plugins by running the function:

```
eiger.enable_plugins()
```

It's also neccessary to prime the plugins by:

```
eiger.prime_plugins()
```

If you need to change the folder in which the h5 and tiff images are stored please use functions from the eiger instance.


{% include-markdown "../../includes/AdvancedBeamlineContent.md" %}

## Accelerator PVs

### BPM (Beam Position Monitor)
- `rdX`: X position readback of the beam.
- `rdY`: Y position readback of the beam.

### BumpProperties (Orbit Bump Properties)
- `BUMPCT4R:setAng`: Set angle for the UE49 bump.
- `BPMZR:angT4H`: Readback angle for the UE49 bump.
- `ABUMPCD5R:setAng`: Set angle for the UE52 bump.
- `BPMZR:angD5H`: Readback angle for the UE52 bump.
- `FSBUMPCR:setAng`: Set angle for the UE56 bump.
- `BPMZR:angD6H`: Readback angle for the UE56 bump.
- `ABUMPCD7R:setAng`: Set angle for the UE112 bump.
- `BPMZR:angD7H`: Readback angle for the UE112 bump.

### BunchProperties (Special Bunch Properties)
- `MEASURECC:CUR:rdCurCS`: Current of the camshaft bunch.
- `MEASURECC:CUR:rdLtCS`: Lifetime of the camshaft bunch.
- `MEASURECC:CUR:setPosCS`: Position setpoint for the camshaft bunch.
- `MEASURECC:CUR:rdCurPPRE1`: Current of the first pre-pre bunch.
- `MEASURECC:CUR:rdLtPPRE1`: Lifetime of the first pre-pre bunch.
- `MEASURECC:CUR:setPosPPRE1`: Position setpoint for the first pre-pre bunch.
- `MEASURECC:CUR:rdCurPPRE2`: Current of the second pre-pre bunch.
- `MEASURECC:CUR:rdLtPPRE2`: Lifetime of the second pre-pre bunch.
- `MEASURECC:CUR:setPosPPRE2`: Position setpoint for the second pre-pre bunch.
- `MEASURECC:CUR:rdCurPTrkX`: Current of the PTrkX bunch.
- `MEASURECC:CUR:rdLtPTrkX`: Lifetime of the PTrkX bunch.
- `MEASURECC:CUR:setPosPTrkX`: Position setpoint for the PTrkX bunch.
- `MEASURECC:CUR:rdCurPTrkY`: Current of the PTrkY bunch.
- `MEASURECC:CUR:rdLtPTrkY`: Lifetime of the PTrkY bunch.
- `MEASURECC:CUR:setPosPTrkY`: Position setpoint for the PTrkY bunch.
- `MEASURECC:CUR:rdCurPTrkZ`: Current of the PTrkZ bunch.
- `MEASURECC:CUR:rdLtPTrkZ`: Lifetime of the PTrkZ bunch.
- `MEASURECC:CUR:setPosPTrkZ`: Position setpoint for the PTrkZ bunch.
- `MEASURECC:CUR:rdCurSL1`: Current of the first slicing bunch.
- `MEASURECC:CUR:rdLtSL1`: Lifetime of the first slicing bunch.
- `MEASURECC:CUR:setPosSL1`: Position setpoint for the first slicing bunch.
- `MEASURECC:CUR:rdCurSL2`: Current of the second slicing bunch.
- `MEASURECC:CUR:rdLtSL2`: Lifetime of the second slicing bunch.
- `MEASURECC:CUR:setPosSL2`: Position setpoint for the second slicing bunch.
- `MEASURECC:CUR:rdCurSL3`: Current of the third slicing bunch.
- `MEASURECC:CUR:rdLtSL3`: Lifetime of the third slicing bunch.
- `MEASURECC:CUR:setPosSL3`: Position setpoint for the third slicing bunch.
- `MEASURECC:CUR:rdCurSL4`: Current of the fourth slicing bunch.
- `MEASURECC:CUR:rdLtSL4`: Lifetime of the fourth slicing bunch.
- `MEASURECC:CUR:setPosSL4`: Position setpoint for the fourth slicing bunch.
- `MEASURECC:CUR:rdCurSL5`: Current of the fifth slicing bunch.
- `MEASURECC:CUR:rdLtSL5`: Lifetime of the fifth slicing bunch.
- `MEASURECC:CUR:setPosSL5`: Position setpoint for the fifth slicing bunch.
- `MEASURECC:CUR:rdCurSL6`: Current of the sixth slicing bunch.
- `MEASURECC:CUR:rdLtSL6`: Lifetime of the sixth slicing bunch.
- `MEASURECC:CUR:setPosSL6`: Position setpoint for the sixth slicing bunch.
- `MEASURECC:CUR:rdCurSL7`: Current of the seventh slicing bunch.
- `MEASURECC:CUR:rdLtSL7`: Lifetime of the seventh slicing bunch.
- `MEASURECC:CUR:setPosSL7`: Position setpoint for the seventh slicing bunch.

### TopUpMonitoring (Top-Up Injection Monitoring)
- `TOPUPCC:estCntDwnS`: Estimated countdown time to the next injection.
- `TOPUPCC:lastShot`: Timestamp of the last injection.

### DetailedAccelerator (Advanced BESSY II Operation State Information)
- `MDIZ3T5G:current`: Current ring current.
- `MDIZ3T5G:lt10`: Overall lifetime of the beam.
- `CUMZR:MBcurrent`: Fill pattern current.
- `TOPUPCC:message`: Top-up message.
- `TOPUPCC:stateName`: Injection state name.
- `FPATCC:mode`: Mode of the fill pattern.
- `TR1SWR:stExpBSfree`: Beamshutter state.
- `MCLKHX251C:freq`: Master clock frequency.
- `OPTICC:mode`: Optics mode.
- `FOFBCC:stAct`: Fast orbit feedback correction status.
- `RMC00V`: Slow orbit correction status.
