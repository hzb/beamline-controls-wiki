This is the cookbook for using bluesky at the UE112 Beamline. Many examples and many commands are explicitly written in a form that is easy for the user/operator to simply copy and paste them into the bluesky terminal as they are or with little modification.

## Start Bluesky

From a terminal write 

`bluesky_start` 

## Start Keithleys Screens
To start the screen for the keithlys run this line in a terminal:

```bash
phoebus_start
```

Then click on the file browser button and select the four Keithleys. You can also detach the screens to be able to see the four of them together. 

## Devices in the Beamline

List of configured devices ("instance name: link to instance class"):

1.  accelerator: [bessyii_devices.ring.DetailedAccelerator](https://codebase.helmholtz.cloud/hzb/bluesky/core/source/bessyii_devices/-/blob/main/bessyii_devices/ring.py?ref_type=heads)

2.  next_injection: [bessyii_devices.ring.TopUpMonitoring](https://codebase.helmholtz.cloud/hzb/bluesky/core/source/bessyii_devices/-/blob/main/bessyii_devices/ring.py?ref_type=heads)

3.  bs: [beamlinetools.devices.beamshutter.Beamshutter](https://codebase.helmholtz.cloud/hzb/bluesky/beamlines_endstations/ue112/source/beamlinetools/-/blob/main/beamlinetools/devices/beamshutter.py?ref_type=heads)

4.  asbl: [beamlinetools.devices.au1.AU1UE112](https://codebase.helmholtz.cloud/hzb/bluesky/beamlines_endstations/ue112/source/beamlinetools/-/blob/main/beamlinetools/devices/au1.py?ref_type=heads)

5.  v3: [bessyii_devices.valve.PositionerBessyValve](https://codebase.helmholtz.cloud/hzb/bluesky/core/source/bessyii_devices/-/blob/main/bessyii_devices/valve.py?ref_type=heads)

6.  v4: [bessyii_devices.valve.PositionerBessyValve](https://codebase.helmholtz.cloud/hzb/bluesky/core/source/bessyii_devices/-/blob/main/bessyii_devices/valve.py?ref_type=heads)

7.  v5: [bessyii_devices.valve.PositionerBessyValve](https://codebase.helmholtz.cloud/hzb/bluesky/core/source/bessyii_devices/-/blob/main/bessyii_devices/valve.py?ref_type=heads)

8.  v6: [bessyii_devices.valve.PositionerBessyValve](https://codebase.helmholtz.cloud/hzb/bluesky/core/source/bessyii_devices/-/blob/main/bessyii_devices/valve.py?ref_type=heads)

9.  v8: [bessyii_devices.valve.PositionerBessyValve](https://codebase.helmholtz.cloud/hzb/bluesky/core/source/bessyii_devices/-/blob/main/bessyii_devices/valve.py?ref_type=heads)

10.  v9: [bessyii_devices.valve.PositionerBessyValve](https://codebase.helmholtz.cloud/hzb/bluesky/core/source/bessyii_devices/-/blob/main/bessyii_devices/valve.py?ref_type=heads)

11. v10: [bessyii_devices.valve.PositionerBessyValve](https://codebase.helmholtz.cloud/hzb/bluesky/core/source/bessyii_devices/-/blob/main/bessyii_devices/valve.py?ref_type=heads)

12. v12: [bessyii_devices.valve.PositionerBessyValve](https://codebase.helmholtz.cloud/hzb/bluesky/core/source/bessyii_devices/-/blob/main/bessyii_devices/valve.py?ref_type=heads)

13. v13: [bessyii_devices.valve.PositionerBessyValve](https://codebase.helmholtz.cloud/hzb/bluesky/core/source/bessyii_devices/-/blob/main/bessyii_devices/valve.py?ref_type=heads)

14. v14: [bessyii_devices.valve.PositionerBessyValve](https://codebase.helmholtz.cloud/hzb/bluesky/core/source/bessyii_devices/-/blob/main/bessyii_devices/valve.py?ref_type=heads)

15. kth1: [bessyii_devices.keithley.Keithley6514](https://codebase.helmholtz.cloud/hzb/bluesky/core/source/bessyii_devices/-/blob/main/bessyii_devices/keithley.py?ref_type=heads)

16. kth2: [bessyii_devices.keithley.Keithley6514](https://codebase.helmholtz.cloud/hzb/bluesky/core/source/bessyii_devices/-/blob/main/bessyii_devices/keithley.py?ref_type=heads)

17. kth3: [bessyii_devices.keithley.Keithley6514](https://codebase.helmholtz.cloud/hzb/bluesky/core/source/bessyii_devices/-/blob/main/bessyii_devices/keithley.py?ref_type=heads)

18. pgm: [beamlinetools.devices.pgm.PGMUE112](https://codebase.helmholtz.cloud/hzb/bluesky/beamlines_endstations/ue112/source/beamlinetools/-/blob/main/beamlinetools/devices/pgm.py?ref_type=heads)

19. m1:  [beamlinetools.devices.m1.M1](https://codebase.helmholtz.cloud/hzb/bluesky/beamlines_endstations/ue112/source/beamlinetools/-/blob/main/beamlinetools/devices/m1.py?ref_type=heads)

20. expchamber: [beamlinetools.devices.experimental_chamber.ExperimentalChamber](https://codebase.helmholtz.cloud/hzb/bluesky/beamlines_endstations/ue112/source/beamlinetools/-/blob/main/beamlinetools/devices/experimental_chamber.py?ref_type=heads)

21. pinhole: [beamlinetools.devices.pinhole.Pinhole](https://codebase.helmholtz.cloud/hzb/bluesky/beamlines_endstations/ue112/source/beamlinetools/-/blob/develop/beamlinetools/devices/pinhole.py?ref_type=heads)


The devices in the beamline have different components. To see the different component of a device use `device.component_names`. For instance, to see the component of the plane grating monochromator, called pgm, use `pgm.component_names` 

In general one can manipulate each component by referring to it as `device.component`. For example the energy of the monochromator is  `pgm.en`.

### Ring: `accelerator`, `next_injection`  

| Component | Description | Units|
| ----- | ------------| ---- |
| `accelerator.current`  | The ring current | mA |
| `next_injection.time_next_injection` | Next injection time | sec |


### Beamshutter: `bs`

| Component | Description | Units|
| ----- | ------------| ---- |
| `bs.status`  | 0=close, 1=open | N.A.|

### FE-Aperture: `asbl`

| Component       | Description        | Units |
| --------------- | ------------------ | -------- |
| `asbl.top`      | Top aperture       | mm |
| `asbl.bottom`   | Bottom aperture    | mm |
| `asbl.left`     | Left aperture      | mm |
| `asbl.right`    | Right aperture     | mm |


### Valves: `v3`, `v4`, `v5`, `v6`, `v8`, `v9`, `v10`, `v12`, `v13`, `v14`  
 
| Component | Description | Units|
| ----- | ------------| ---- |
| `v3.setpoint`  | 0=close, 1=open | N.A.|
| `v3.readback`  | 0=close, 1=open | N.A.|
<!--
| `v?.status`  | 0=close, 1=open | N.A.|
| `v?.toggle`  | 0=close, 1=open | N.A.|
| `v?.done`  | 0=False, 1=True | N.A.| 
-->


### Keithleys: `kth1`, `kth2`, `kth3`

| Component           | Description        | Units |
| --------------- | ------------------ | -------- |
| `kth1.current` | Model 6514 | A |
| `kth2.current` | Model 6514 | A |
| `kth3.current` | Model 6514 | A |


### Plane Grating Monochromator: `pgm` 

| Component  | Description | Units |
| ---------  | --------------------- | -------- |
| `pgm.alpha` | Grating incoming |deg|
| `pgm.beta`  | Grating outgoing |deg|
| `pgm.theta` | Mirror angle |deg|
| `pgm.en`    | Energy| eV |
| `pgm.slit`  | Exit slit |um|

#### PGM - ID Control

- pgm.ID_on.put(1) sets the ID to state 'ON'
- pgm.ID_on.put(0) sets the ID to state 'OFF'
- pgm.ID_on.get() returns the current value (0 or 1)

### Mirror: `m1`

| Component  | Description | Units |
| ---------- | -------------------------- | -------- |
| `m1.tx`    | Translation in X           |mm|
| `m1.rx`    | Rotation about X axis      |mm|
| `m1.ry`    | Rotation about Y axis      |mm|
| `m1.rz`    | Rotation about Z axis      |mm|

### Experimental Chamber: `expchamber` 

| Component  | Description                | Units |
| ---------- | -------------------------- | -------- |
| `expchamber.x`    | Translation in X      |mm|
| `expchamber.y`    | Translation in Y      |mm|
| `expchamber.z`    | Translation in Z      |mm|
| `expchamber.r`    | Rotation about Z axis |deg|

### Pinhole: `pinhole` 

| Component  | Description                | Units |
| ---------- | -------------------------- | -------- |
| `pinhole.h` | Horizontal |mm|
| `pinhole.v` | Vertical |mm|

## Move motors and set values

### Move motors
If you want to move a motor, or a set of motors you can simply type:

`mov motor1 position1 motor2 position2`

or using relative motions

`movr motor1 relative_motion1 motor2 relative_motion2`

<!-- A progress bar will be shown, the last number on the right is the current motor position (this works when you move only one motor)

![Progress Bar](../assets/images/progress_bar_opticBL.png) -->

### Set Values
If you want to set a value, for instance a new cff for the monochromator, use the following syntax

`device.axis.set(value)`

## Change the plange grating monochromator cff

To set the cff value to 2.5 use:

`sgm.cff.put(2.5)`

Note that this will not produce any movement in the monochromator. The new cff will be set once you change the energy to a new one.

## Magics
* `ct` will show a reading of all the detectors
* `wa` will show all the motor positions. You can select which motors to show using their label. For instance `wa reflectometer` will show only the reflectometer motor positions.
* `where` Draws a line of the current motor position in the last scan.
* `pic` Move the motor at the highest value of the signal from the detector in the last scan
* `cen` Move the motor at the center of the signal from the detector in the last scan
* `com` Move the motor at the center of mass of the signal from the detector in the last scan
* `minimum` Move the motor at the minimum of the signal from the detector in the last scan
* `bec.peaks` will show info about the peak in the last scan, if any

## Data

### Specfile
The data are exported in a spec file, located in `/home/opticsmaster/bluesky/data/beamline_commissioningYEAR`.

### Individual csv files
Additionally, individual csv files are exported in `/home/opticsmaster/bluesky/data/beamline_commissioning/csv`.

### Change User
To change the folder where the single csv files are exported use the following command:

* `changeuser("user_name")`

This will create a folder called `/home/ue112/bluesky/data/user_name/csv` where the files will be exported.

Three kind of files will be exported for each scan:

* **scanNumber_baseline.csv**: here are contained the measured values for all the devices in the baseline (measured once before and once after the scan)
* **scanNumber_meta.json**: here are contained the metadata relative to the scan
* **scanNumber_primary.csv**: here are contained the data for the scan. 



## Scan Types

Several scan types are available by default in Bluesky. For a complete list and the documentation refer to the  [official documentation](https://blueskyproject.io/bluesky/plans.html)

### Original syntax
Plans have to be passed as an argument to the Run Engine, and they are functions themselfs. For instance the count plan should be invoked like this: `RE(count([noisy_det],5,delay=1))`

### Simplified syntax
Experimental ipython magics are available that aim to simplify the syntax. For instance, the same count plan as above can be invoked like this: `count [noisy_det] 5 delay=1`

In case of problems, please report it to the beamline scientist and use the original syntax.

* `count`: take one or more readings from detectors. Usage (5 counts, 1 sec delay): 
    * `count [noisy_det] 5 delay=1`

* `scan` or `ascan`: scan over one multi-motor trajectory. Usage (scan motor1 between -1 and 1 in 10 points) 
    * `ascan [noisy_det] motor1 -1 1 10`

* `relative_scan` or `dscan`: scan over one multi-motor trajectory relative to current position. Usage (scan motor1 between -1 and 1 in 10 points) 
    * `dscan [noisy_det] motor1 -1 1 10`

* `list_scan`: scan over one or more variables in steps simultaneously (inner product). Usage (scan motor1 on a list of given points) 
    * `dscan [noisy_det] motor1 [point1, point2, ...]`

## Average measurement for each point
When measuring with the Keithleys, it is possible to instruct them to repeat the measurement *n* times, and return the averaged value.  

To do this we have to set the parameter avg_num of the Keithley to the desired value, by default is 1. For instance, to change it to 5, for each Keithley, give the following commands

```python
kth1.avg_type.set('Repeat')
kth1.avg_num.put(5)

kth2.avg_type.set('Repeat')
kth2.avg_num.put(5)

kth3.avg_type.set('Repeat')
kth3.avg_num.put(5)

kth4.avg_type.set('Repeat')
kth4.avg_num.put(5)
```

## Settle Time (wait before measuring)
It is possible to define a settle time, which is a a number of seconds waited by a motor before reporting that the motion is completed.  For instance, to set the settle time of the `pgm.en` axis, use:

```python
pgm.en.settle_time = 5
```
### Settle Time for all motors
If interesting can be implemented


## Add metadata to the scans
Custom metadata can be entered by the user at the execution time of a plan. Suppose that we are exectuing a plan called `scan()` (for brevity we omit the parameters that we need to pass to the scan function). One can add the metadata either like this:

* `RE(scan(), operator='John', sample='pure_gold')`


Another way is to add the metadata directly in the plan. In this case we need to use a python dictionary.

* `RE(scan([det], motor, 1, 5, 5, md={'operator':'John', 'sample':'gold'}))`

or using the symplified syntax:

* `scan [det] motor 1 5 5 md={'operator':'John','sample':'gold'}`


## Abort Motion
Weather you are moving a motor or running a plan, the motion can be interrupted by pressing `ctrl+c`.

<!-- This will work for the `hios` and the `pgm.en` motors.  -->

## Suspenders
Bluesky can automatically pause a measurement in case some conditions are met, by using `suspenders`. Two suspenders can be activated.

<!-- ### wait_for_injection
This suspender pause the measurements two seconds before and after the injection happens. To activate it and deactivate it use

```python
wait_for_injections()
```
-->

### check_beamshutter
This suspender pause the measurements if it detects that the beamshutter or the last valve are closed. To activate it and deactivate it use

```python
check_beamshutter()
```

## How to write a script
Make a folder with your name in `/home/opticsmasters/bluesky/user_scripts/your_name`. You can then write your scripts in that folder. Examples are available in that folder `/home/opticsmasters/bluesky/user_scripts/examples`

The commands that are normally written in the ipython terminal should be passed as an argument to the function `run_plan()`, as in the examples below.

One can write either procedural scripts, where each line will be executed one after the other one like in the example below. To run this script just type `load_user_script('examples/example_script.py'`

```python
# print the pocurrent energy of the monochromator
print(f"Current position of the PGM {pgm.en.get()}")

# move the energy to 403 eV
run_plan("mov pgm.en 403")

# run a scan
run_plan("dscan [noisy_det] motor -1 1 10")
```

Or it is possible to define functions to call from the command line. To run this script just type `load_user_script('examples/example_with_functions.py')`. Now the function `test_plan()` is available in the ipython session, so just type `test_plan()` to execute it

```python
def test_plan():
    # print the pocurrent energy of the monochromator
    print(f"Current position of the PGM {pgm.en.get()}")

    # move the energy to 403 eV
    run_plan("mov pgm.en 403")

    # run a scan
    run_plan("dscan [noisy_det] motor -1 1 10")
```

## Moxa Box
It is registered in the DNS/DHCP with name: `ue112pgm1-moxa01.exp1303.bessy.de` and address `172.31.198.60`

## Portainer
Portainer is availbale at [this link](http://172.18.0.2:9000/#!/auth).
<!-- Username: -->
<!-- Psw: -->

## Start the container: 'userpositioner'
* Attach a shell to the container: `userpositioner` In Visual Code.
* In the shell, change to the directory: `/opt/epics/ioc/UserPositionerIOC/iocboot/iocUserPositioner`
* Run from the shell: `../../bin/linux-x86_64/UserPositionerIOC st.cmd`

## Experts Section
If you are a user, this section is not for you. 

## Additional command to start bluesky
Bluesky is running in containers. To restart the containers use:

`bluesky_start restart` 

To bring down the containers:

`bluesky_start down` 

To enter the container to debug:

`bluesky_start sh` 

### Containers Running on the local machine
This section describes which containers are running on the local machine. If you are an user, this section is of no use for you.

To visualize the running containers, you can use either Portainer (see section above), or VSCode. 

#### Bluesky
* bluesky (The container containing the bluesky environment)
* Mongo (the database where bluesky saves the data)
* Tiled (a data-access service used to connect to the mongoDB catalog)
* Graphana (to monitor and PVs with a GUI, not necessary to run bluesky)
* MongoExpress (to monitor the usage of the MongoDB)
* Portainer (to visualize/mange the running containers) 

#### EPICS
<!-- * Keithley IOC (this is the IOC used to connect to the keithleys) -->
<!-- * UserValues reflectometer (this containers reads the PV exposed by the labview program and exposes a new set of PV with a simpler logic to implement in Ophyd) -->
* Phoebus (this is the container with phoebus, we use is to visualize the keithleys screens)
