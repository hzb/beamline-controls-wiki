When transitioning between Spec and Bluesky software for controlling experiments, adjustments are needed for interfacing with Keithley meters. These meters use a GPIB cable with Spec and an RS232 port with Bluesky.

### Switching from Spec to Bluesky

#### Preparation
Before switching from Spec to Bluesky:
- **Take a photo of the GPIB connections** to ensure you can reconnect them correctly when switching back.

#### Steps for Switching
1. **Disconnect the GPIB cable** from each Keithley meter.
2. **Connect the RS232 cable (Moxa)** to the corresponding ports:
   - Keithley 1: port 1
   - Keithley 2: port 3
   - Keithley 3: port 5
   - Keithley 4: port 7

#### Configuration for Each Keithley Model

##### Keithley 6514
- Turn off GPIB communication.
- Change the communication protocol to RS232.
- Set the baud rate to 11920.
- Change the terminator setting from CR to CRLF.
- Restart the device.

##### Keithley 6517B
- Change the communication protocol from GPIB to RS232.
- Set the baud rate to 19200.
- Change the terminator setting from CR to CRLF.
- Restart the device.

### Switching from Bluesky to Spec

To revert to using Spec after operating with Bluesky:

1. **Disconnect the RS232 cables** from all Keithley meters.
2. **Reconnect the GPIB cables** according to the positions noted in the photos taken during the initial switch.
3. **Configure each Keithley meter** to use GPIB communication by reversing the changes made when switching to Bluesky.
