# Cookbook

This is the cookbook for using bluesky at the Optics Beamline. Many examples and many commands are explicitly written in a form that is easy for the user/operator to simply copy and paste them into the bluesky terminal as they are or with little modification.

{%
    include-markdown "../../../../common/start_bluesky.md"
%}

{%
    include-markdown "../../../../common/start_phoebus.md"
%}

{%
    include-markdown "../../../../common/move_motors.md"
%}
On the reflectometer one can use the axis using absolute cordinates

```python
mova r.axes absolute_position
```

{%
    include-markdown "../../../../common/change_cff.md"
%}

{%
    include-markdown "../../../../common/scan_with_magics.md"
%}

## PTB scan
This scan let you choose how many times you want to mesure one single point, by settin the parameter repeat. One can add as many motor as desired, with the following syntax:

```python
ptb_scan motor1 start1 stop1 motor2 start2 stop2 n_points repeat=2

# example of a scan with one motor
ptb_scan motor -1 1 10 repeat=2

# example of a scan with two motors
ptb_scan motor -1 1 motor2 -2 2 10 repeat=2
```

{%
    include-markdown "../../../../common/scan_continuous.md"
%}
This scan will work with the following motors: `fsu`.
{%
    include-markdown "../../../../common/scan_continuous_energy.md"
%}

{%
    include-markdown "../../../../common/settle_time.md"
%}
**Settle Time for all motors**

The following function will change the value of the settle time for all the motors to the desired value. For instance to change it to 2 or to 5 seconds use one of the following

```python
settle_time(2)

settle_time(5)
```

## Autohios
It is possible to instruct bluesky to automatically set the High Order Suppressor (HIOS) motors to the desired values depending on the selected energy of the monochromator. There are three tables that can be used to select the HIOS motor positions. The tables are located in the folder `~/bluesky/beamlinetools/beamlinetools/hios`


To activate autohios

```python
autohios(1) # to select table number one

autohios(2) # to select table number two

autohios(3) # to select table number three
```

T0 deactivate autohios

```python
autohios(0) 

```

## Keithleys

### Average
When measuring with the Keithleys, it is possible to instruct them to repeat the measurement *n* times, and return the averaged value.  

```python
kth1.avg_type.set('Repeat')
kth1.avg_num.put(5)
```

### Triggers before reading
By default at the Optics beamline the keithleys are triggered three times before being read. Once can change the number of times a keithley is triggered before being read setting the trigger repetion axis. For instance, for keithley 1:

```python
kth1.trigger_rep.set(5)
```

{%
    include-markdown "../../../../common/scan_metadata.md"
%}

{%
    include-markdown "../../../../common/abort_motion.md"
%}

{%
    include-markdown "../../../../common/magics.md"
%}

{%
    include-markdown "../../../../common/data_management.md"
%}




## Suspenders
Bluesky can automatically pause a measurement in case some conditions are met, by using `suspenders`. 

{%
    include-markdown "../../../../common/suspender_injection.md"
%}

## Change Sample
One can save the coordinate of the tripod and goniometer for sample changing. The first time drive the sample to the change position manually and once you are happy with the coordinate save them using:

```python
save_sample_position('my_positions')
```

To drive the tripod and goniometer to the same set of coordinates use:

```python
# to use the default positions
sample_change()

# to use custom positions
sample_change('my_positions')
```

Finally, to start measuring:

```python
sample_measure()
```

