# PTB cookbook

This page contains a summary of the most import commands for Analia's experiment. 

## Change Sample
One can save the coordinate of the tripod and goniometer for sample changing. The first time drive the sample to the change position manually and once you are happy with the coordinate save them using:

```python
save_sample_position('my_positions')
```

To drive the tripod and goniometer to the same set of coordinates use:

```python
# to use the default positions
sample_change()

# to use custom positions
sample_change('my_positions')
```

Finally, to bring the sample to the measuring positions:

```python
sample_measure()
```

## Settle Time
All the motors at the optics beamline have a settle time (a time they wait after the movement is finished before they repor done) of 0.5 seconds. This settle time introduces a delay between the end of the movement and the beginning of the measurement. The delay is expressed in seconds

```python
# settle time for all motors of 2 seconds
settle_time(2)
```

## Keithleys
You can use the gui to visualize the Keithleys on the measuring screen. 

### Average
When measuring with the Keithleys, it is possible to instruct them to repeat the measurement *n* times, and return the averaged value.  

```python
kth1.avg_type.set('Repeat')
kth1.avg_num.put(5)
```

### Triggers before reading
By default at the Optics beamline the keithleys are triggered three times before being read. Once can change the number of times a keithley is triggered before being read setting the trigger repetion axis. For instance, for keithley 1:

```python
kth1.trigger_rep.set(5)
```

## Autohios
It is possible to instruct bluesky to automatically set the High Order Suppressor (HIOS) motors to the desired values depending on the selected energy of the monochromator. There are three tables that can be used to select the HIOS motor positions. The tables are located in the folder `~/bluesky/beamlinetools/beamlinetools/hios`


To activate autohios

```python
# activate autohios
autohios(1) 
```

To deactivate autohios

```python
autohios(0) 
```

## Scan Examples
A few examples, see the Cookbook for a more comprehensive list of scans available.

### Select detector to plot live
```python
# use this command and follow the instructions
plotselect()
```

### PTB scan
This scan let you choose how many times you want to mesure one single point, by setting the parameter repeat to the desired value. One can add as many motor as desired, with the following syntax:

```python
ptb_scan motor1 start1 stop1 motor2 start2 stop2 n_points repeat=2

# example of a scan with one motor
ptb_scan motor -1 1 10 repeat=2

# example of a scan with two motors
ptb_scan motor -1 1 motor2 -2 2 10 repeat=2
```

### Energy Scan

```python
# scan motor start stop number_of_steps
scan pgm.en 400 600 100
```

### Relative scan

```python
# dscan motor start stop number_of_steps
dscan r.tx -1 1 20
```

### Absolute scan

```python
# scan motor start stop number_of_steps
scan r.tha 0 40 100
```

## Scripting

Yuu can write scripts in the folder `~/bluesky/user_scripts/ptb`, and then load them with:

```python
load_user_scripts('ptb/ptb_example.py')
```