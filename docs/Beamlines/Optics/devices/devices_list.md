{%
    include-markdown "../../../../common/component_names.md"
%}

{%
    include-markdown "../../../../common/accelerator.md"
%}


### Beamshutter
label: beamshutter

| Motor           | Description        | Units |
| --------------- | ------------------ | -------- |
| `bs.status`      | close:0, open:1   | na |

### Water Apertures  
label: apertures

| Motor           | Description        | Units |
| --------------- | ------------------ | -------- |
| `wau1.top`      | Top aperture       | mm |
| `wau1.bottom`   | Bottom aperture    | mm |
| `wau1.left`     | Left aperture      | mm |
| `wau1.right`    | Right aperture     | mm |


### M1
label: mirrors

| Motor      | Description                | Units |
| ---------- | -------------------------- | -------- |
| `m1.tx`    | Translation in X           |µm|
| `m1.ty`    | Translation in Y           |µm|
| `m1.tz`    | Translation in Z           |µm|
| `m1.rx`    | Rotation about X axis      |rad|
| `m1.ry`    | Rotation about Y axis      |rad|
| `m1.rz`    | Rotation about Z axis      |rad|

### Plane Grating Monochromator  
label: pgm

| Motor            | Description                    | Units |
| ---------        | ---------------------          | -------- |
| `pgm.en`         | Energy                         |eV|
| `pgm.cff`        | Constant fix focus factor      |a.u.|
| `pgm.diff_order` | Diffraction Order              |a.u.|
| `pgm.slit`       | Exit Slit width                |µm|
| `pgm.grating`    | The grating that is being used |l/mm|
| `pgm.alpha`      | The incident entrance angle on the grating |deg|
| `pgm.beta`       | The incident exit angle on the grating |deg|
| `pgm.theta`      | The incident angle on the mirror |deg|

### High Order Suppressor
label: hios

The motor controlling the second couple of crystals is implemented using a psudo motor, to be able to change the offset. This is done because we found out that when both motors are set at 0, to have them really at a specular position there is a n offset on the second motor. 

| Motor      | Description           |Units |
| ---------- | --------------------- | -------- |
| `hios.htr` | Horizontal motor 3    |mm|
| `hios.hm1` | Horizontal motor 1    |deg|
| `hios.hm2pseudo.hm2` | Horizontal motor 2, with offset    |deg|
| `hios.hm2pseudo.rhm2` | Horizontal motor 2, absolute    |deg|

The following shortcuts are available for the psudomotors. However, when the data is saved, the motors will have the names as above. 

| Shortcut      | Real Name           
| ---------- | --------------------- |
| `hios.hm2pseudo.hm2` |  `hios.hm2` |
| `hios.hm2pseudo.rhm2` | `hios.hm2_absolute` |


#### Change Offset of hios.hm2

The offset value is stored in a file contained in `/home/opticsmaster/bluesky/beamlinetools/beamlinetools/hios/offset.txt`. At the moment this guide was written `offset=0.88°`. It can be read anytime from bluesky issuing the following command

```python
hios.read_offset()
```

and it can be changed using this command:
```python
hios.save_new_offset(new_value)
```

Note that the change effect the next time the `hm2` motor is moved, and it is persistent across different bluesky sessions (If you close and reopen bluesky the new offset value is used).

### Filter Slit Unit  
label: fsu

| Motor      | Description          | Units |
| ---------- | -------------------- | -------- |
| `fsu.slt1` | Slit 1 position      |mm|
| `fsu.flt1` | Filter 1 position    |mm|
| `fsu.slt2` | Slit 2 position      |mm|
| `fsu.flt2` | Filter 2 position    |mm|

#### Set Slit
Two functions are available to set either slit1 or slit2.

```python
Slit1()

Slit2()
```

both functions will save the the current Slit in use in the RunEngine metadata.

```python
# to check the current Slit1
RE.md['current_Slit1']

RE.md['current_Slit2']
```

In case the Slit were moved using blue panels or without using the proper functions, the metadata can be manually changed to match the slit in use

```python
# for instance, to set Slit1 to use the Slit number 2
RE.md['current_Slit1']=2

# for instance, to set Slit2 to use the Slit number 3
RE.md['current_Slit2']=3
```

#### Set Filter
Two functions are available to set either slit1 or slit2.

```python
Filter1()

Filter2()
```

both functions will save the the current Slit in use in the RunEngine metadata.

```python
# to check the current Filter1
RE.md['current_Filter1']

# to check the current Filter2
RE.md['current_Filter2']
```

In case the filter were moved using blue panels or without using the proper functions, the metadata can be manually changed to match the filter in use

```python
# for instance, to set Filter2 to use the Filter number 2
RE.md['current_Filter1']=2

# for instance, to set Filter2 to use the Filter number 3
RE.md['current_Filter2']=3
```
### Reflectometer  
label: reflectometer

| Motor      | Description            | Units |
| ---------- | ---------------------- | -------- |
| `r.tx`     | Translation in X       |mm|
| `r.ty`     | Translation in Y       |mm|
| `r.tz`     | Translation in Z       |mm|
| `r.rx`     | Rotation about X axis  |deg|
| `r.ry`     | Rotation about Y axis  |deg|
| `r.rz`     | Rotation about Z axis  |deg|
| `r.phi`    | Phi angle              |deg|
| `r.tha`    | Theta angle            |deg|
| `r.twt`    | Tweeter                |deg|
| `r.det1`   | Detector 1             |mm|
| `r.det2`   | Detector 2             |mm|

#### Set an axis to zero/custom value
Each axis of the reflectometer, can be set to zero, or to a custom value. For instance, to set to zero `r.tha` use the following syntax:

```python
r.tha.set_to_zero()
```

To set it to a custom value use

```python
r.tha.set_current_position(value)
```

#### Set Detector
Two functions are available to set either slit1 or slit2.

```python
Detector()

# or if you already know what detector to use, for instane for detector number 5
Detector(target_device_index=5)
```

both functions will save the the current Detector in use in the RunEngine metadata.

```python
# to check the current Detector
RE.md['current_Detector']
```

In case the Detecotr were moved using blue panels or without using the proper functions, the metadata can be manually changed to match the detector in use

```python
# for instance, to set detector to use the detector number 2
RE.md['current_Detector']=2

```

#### Set all the axis to zero
This command will set all the axis of the reflectometer to zero, excluded `r.det1` and `r.det2`

```python
r.set_to_zero()
```

#### Set all the goniometer axis to zero
This command will set all the axis of the reflectometer to zero, excluded `r.det1` and `r.det2`

```python
r.set_gon_to_zero()
```

#### Initialize axis
If the IOC of the reflectometer gets turned off, or the IOC acting as a man in the middle get turned off, it is necessary to initialise the axis (this is due to the fact that there is no readback value for some axis). To do this, using the Labview program set 
* `r.tx`=0
* `r.ty`=0
* `r.tz`=0
* `r.rx`=0
* `r.ry`=0
* `r.rz`=0

and the issue in bluesky the following command:

```python
r.initialise_axes()
```

### Keithleys
label: keithley
Existing devices: 
* Model 6517B: `kth1`, `kth2`
* Model 6514: `kth3`, `kth4`

| Device           | Description        | Units | Values |
| --------------- | ------------------ | -------- | -------- |
| `kth1.readback`      | current   | Amp |  |
| `kth1.rnge`      | range   | n.a. | |
| `kth1.nplc`      | number of power line cycles   | n.a. |0.01 (fast) to 10 (accurate) |
| `kth1.int_time`      | integration time   | n.a. ||
| `kth1.avg_num`      | number of averages    | n.a. |1-100 readings|
| `kth1.avg_type`      | average type    | n.a. |0:moving average, 1:repeating average|
| `kth1.vsrc_ena`      | voltage enabled (only 6517B)| n.a. |0:no, 1:yes|
| `kth1.vsrc`      | voltage (only 6517B) | n.a. |40 V to 400 V|

### Valves  
label: valves
Each valve has readback, see table below. 

| Motor           | Description         | Values |
| --------------- | ------------------ | -------- |
| `v3`| valve 3 | 0: close, 1: open|
| `v4`| valve 4 | 0: close, 1: open|
| `v5`| valve 5 | 0: close, 1: open|
| `v6`| valve 6 | 0: close, 1: open|
| `v7`| valve 6 | 0: close, 1: open|
| `v8`| valve 8 | 0: close, 1: open|
| `v9`| valve 9 | 0: close, 1: open|
| `v10`| valve 10 | 0: close, 1: open|
| `v11` | valve 11 | 0: close, 1: open|
| `v13` | valve 13 | 0: close, 1: open|

