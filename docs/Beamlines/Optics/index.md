# Welcome to the beamline documentation

<style>
body {
  font-family: Arial, sans-serif;
  background: linear-gradient(rgba(255, 255, 255, 0.8), rgba(255, 255, 255, 0.8)), 
              url('https://example.com/background-image.jpg');
  background-size: cover;
  background-position: center;
}

.grid-container {
  display: grid;
  grid-template-columns: 1fr 1fr;
  grid-template-rows: 1fr 1fr;
  grid-gap: 30px;
  text-align: center;
  padding: 60px;
  font-size: 1.2em;
}

.grid-item {
  padding: 40px;
  border: 2px solid #ddd;
  border-radius: 15px;
  box-shadow: 0px 4px 10px rgba(0, 0, 0, 0.1);
  transition: transform 0.3s ease, box-shadow 0.3s ease;
  background-color: rgba(255, 255, 255, 0.85);  /* Transparent background to blend with the page */
  backdrop-filter: blur(10px); /* Adds a glass-like effect */
  text-align: left; /* Align text to the left */
}

.grid-item:hover {
  transform: translateY(-10px);
  box-shadow: 0px 6px 12px rgba(0, 0, 0, 0.2);
}

.grid-item strong {
  font-size: 1.6em;
  color: #333;
}

.grid-item em {
  color: #ff7043;
  font-weight: 600;
  font-size: 1.3em;
}

.grid-item a {
  color: #1e88e5;
  text-decoration: none;
  font-weight: 500;
  margin-top: 10px;
  display: inline-block;
  transition: color 0.2s ease;
  text-align: left; /* Align text to the left */
}

.grid-item a:hover {
  text-decoration: underline;
  color: #005cb2;
}

.grid-item a::before {
  content: "➡ ";
}

/* Additional styling for page */
h1 {
  font-size: 2.5em;
  text-align: center;
  color: #3c3c3c;
  margin-bottom: 40px;
}

h1::after {
  content: '';
  display: block;
  width: 50px;
  height: 4px;
  background-color: #ff7043;
  margin: 10px auto;
}
</style>

<div class="grid-container">
  <div class="grid-item">
    <strong>How-To Guides</strong><br>
    <a href="how-to/cookbook">Cookbook</a><br>
    <a href="how-to/ptb">PTB Cookbook</a><br>
    <a href="how-to/write_a_script">Write a Script</a><br>
    <a href="how-to/write_a_plan">Write a Plan</a><br>
    <a href="how-to/spec_to_bluesky">Switch to Bluesky</a><br>
  </div>

  <div class="grid-item">
    <strong>Devices</strong><br>
    <a href="devices/instantiate_devices">Instantiate Devices</a><br>
    <a href="devices/devices_list">Devices List</a><br>
  </div>

  <div class="grid-item">
    <strong>Tutorials</strong><br>
    <a href="tutorials/python_tutorial">Python Tutorial</a><br>
    <a href="tutorials/use_bluesky_native_syntax">Use Bluesky Native Syntax</a><br>
  </div>

<div class="grid-item">
    <strong>Useful Links</strong><br>
    <a href="https://blueskyproject.io/" target="_blank">Bluesky Project</a><br>
    <a href="https://blueskyproject.io/bluesky/main/userindex.html" target="_blank">Bluesky User Documentation</a><br>
    <a href="https://blueskyproject.io/tutorials/README.html" target="_blank">Bluesky Tutorials</a><br>
    <a href="https://blueskyproject.io/ophyd/user/index.html" target="_blank">Ophyd User Documentation</a><br>
    <a href="https://codebase.helmholtz.cloud/hzb/bluesky/core/source/bessyii_devices" target="_blank">bessyii_devices package</a><br>
    <a href="https://codebase.helmholtz.cloud/hzb/bluesky/core/source/bessyii" target="_blank">bessyii package</a><br>
</div>

</div>
