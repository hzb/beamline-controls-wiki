This is the cookbook for using bluesky at the Belchem Beamline. Many examples and many commands are explicitly written in a form that is easy for the user/operator to simply copy and paste them into the bluesky terminal as they are or with little modification.

{%
    include-markdown "../../../../common/start_bluesky.md"
%}

{%
    include-markdown "../../../../common/start_phoebus.md"
%}

* To open the keithley screens open the following files:
`/opt/phoebus/gui/keithleys/kth01.bob`

* To open the beamline screen open the following files:
`/opt/phoebus/gui/belchem/beamline.bob`

{%
    include-markdown "../../../../common/move_motors.md"
%}

{%
    include-markdown "../../../../common/change_cff.md"
%}

{%
    include-markdown "../../../../common/scan_with_magics.md"
%}

{%
    include-markdown "../../../../common/scan_continuous.md"
%}

{%
    include-markdown "../../../../common/scan_continuous_energy.md"
%}

{%
    include-markdown "../../../../common/scan_metadata.md"
%}

{%
    include-markdown "../../../../common/abort_motion.md"
%}

{%
    include-markdown "../../../../common/magics.md"
%}

{%
    include-markdown "../../../../common/data_management.md"
%}


{%
    include-markdown "../../../../common/average_keithleys.md"
%}


## Suspenders
Bluesky can automatically pause a measurement in case some conditions are met, by using `suspenders`. 

{%
    include-markdown "../../../../common/suspender_injection.md"
%}

<!-- {%
    include-markdown "../../../../common/suspender_beamshutter.md"
%} -->
