{%
    include-markdown "../../../../common/component_names.md"
%}

{%
    include-markdown "../../../../common/accelerator.md"
%}

### Undulator
label: undulator

| Motor           | Description        | Units |
| --------------- | ------------------ | -------- |
| `u49.gap`        | gap of the undulator   | mm |
| `u49.id_control` | IDon: 1, IDoff:0   |  |

### Beamshutter
label: beamshutter

| Motor           | Description        | Units |
| --------------- | ------------------ | -------- |
| `bs.status`      | close:0, open:1   | na |

### Aperture 1
label: apertures

| Motor           | Description        | Units |
| --------------- | ------------------ | -------- |
| `au1.top`      | Top aperture       | mm |
| `au1.bottom`   | Bottom aperture    | mm |
| `au1.left`     | Left aperture      | mm |
| `au1.right`    | Right aperture     | mm |

### M1
label: mirrors

| Motor      | Description                | Units |
| ---------- | -------------------------- | -------- |
| `m1.tx`    | Translation in X           |µm|
| `m1.ty`    | Translation in Y           |µm|
| `m1.rx`    | Rotation about X axis      |rad|
| `m1.ry`    | Rotation about Y axis      |rad|
| `m1.rz`    | Rotation about Z axis      |rad|
| `m1.temperature`    | temperature      |°C|

### Plane Grating Monochromator  
label: pgm

| Motor            | Description                    | Units |
| ---------        | ---------------------          | -------- |
| `pgm.en`         | Energy                         |eV|
| `pgm.cff`        | Constant fix focus factor      |a.u.|
| `pgm.diff_order` | Diffraction Order              |a.u.|
| `pgm.slit`       | Exit Slit width                |µm|
| `pgm.grating`    | The grating that is being used |l/mm|
| `pgm.alpha`      | The incident entrance angle on the grating |deg|
| `pgm.beta`       | The incident exit angle on the grating |deg|
| `pgm.theta`      | The incident angle on the mirror |deg|

### Pinhole
label: pinhole

| Motor           | Description        | Units |
| --------------- | ------------------ | -------- |
| `ph.h`      | Horizontal translation   | mm |
| `ph.v`      | Vertical translation   | mm |

### Keithleys
label: keithley

| Motor           | Description        | Units | Values |
| --------------- | ------------------ | -------- | -------- |
| `kth01.readback`      | current   | Amp |  |
| `kth01.rnge`      | range   | n.a. | |
| `kth01.nplc`      | number of power line cycles   | n.a. |0.01 (fast) to 10 (accurate) |
| `kth01.int_time`      | integration time   | n.a. ||
| `kth01.avg_num`      | number of averages    | n.a. |1-100 readings|
| `kth01.avg_type`      | average type    | n.a. |0:moving average, 1:repeating average|
| `kth01.vsrc_ena`      | voltage enabled    | n.a. |0:no, 1:yes|
| `kth01.vsrc`      | voltage     | n.a. |40 V to 400 V|

### Valves  
label: valves
Each valve has readback, see table below. 

| Motor           | Description         | Values |
| --------------- | ------------------ | -------- |
| `v3`| valve 3 | 0: close, 1: open|
| `v4`| valve 4 | 0: close, 1: open|
| `v5`| valve 5 | 0: close, 1: open|
| `v6`| valve 6 | 0: close, 1: open|
| `v8`| valve 8 | 0: close, 1: open|
| `v11` | valve 11 | 0: close, 1: open|
| `v12` | valve 12 | 0: close, 1: open|
| `v13` | valve 13 | 0: close, 1: open|
| `v14` | valve 14 | 0: close, 1: open|
| `v15` | valve 15 | 0: close, 1: open|


### Beamline Monitor
This is a collection of all the diagnostic we have on the beamline. They are all Read Only.

| Motor           | Description        | Units |
| --------------- | ------------------ | -------- |
| `bm.front`      | Pressure   | ... |
| `bm.m1`      | Pressure   | ... |
| `bm.mono`      | Pressure   | ... |
| `bm.m3`      | Pressure   | ... |
| `bm.ion`      | Pressure   | ... |
| `bm.m4`      | Pressure   | ... |
| `bm.diag`      | Pressure   | ... |
| `bm.sec3.pressure`      | Pressure   | ... |
| `bm.sec3.voltage`      | Voltage   | ... |
| `bm.sec4.pressure`      | Pressure   | ... |
| `bm.sec4.voltage`      | Voltage   | ... |
| `bm.sec5.pressure`      | Pressure   | ... |
| `bm.sec5.voltage`      | Voltage   | ... |
| `bm.sec6.pressure`      | Pressure   | ... |
| `bm.sec6.voltage`      | Voltage   | ... |
| `bm.sec7.pressure`      | Pressure   | ... |
| `bm.sec7.voltage`      | Voltage   | ... |
| `bm.sec8.pressure`      | Pressure   | ... |
| `bm.sec8.voltage`      | Voltage   | ... |
| `bm.sec10.pressure`      | Pressure   | ... |
| `bm.sec10.voltage`      | Voltage   | ... |
| `bm.sec11.pressure`      | Pressure   | ... |
| `bm.se11.voltage`      | Voltage   | ... |

