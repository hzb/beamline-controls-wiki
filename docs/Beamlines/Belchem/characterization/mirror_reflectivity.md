ALl the mirrors in the beamline are coated with Platinum and they have a grazing incident angle of 1.5°.

* m1: toroidal mirror collimating vertically and focusing horizontally on the exit slit
* m2: pre-mirror of the SX-700 monochromator
* m3: cylindrical mirror focusing vertically on the exit slits
* m4: Toroidal mirror focusing on the sample position

![alt text](../../images/belchem/mirror_reflectivity.png)