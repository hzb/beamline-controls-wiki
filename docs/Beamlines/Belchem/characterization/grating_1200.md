This is an overview of the performance of the Belchem beamline using the 1200 l/mm grating.

* Source UE49:
* Grating: 1200 l/mm blazed grating
* Exit Slit Opening: 20 μm

The simulations are realized using WAVE files for the undulator, and they are divided in Harmonics (1st, 3rd and 5th), and for different cff values (1.6, 2.25, 5, 10)

![alt text](../../images/belchem/grating_1200.png)